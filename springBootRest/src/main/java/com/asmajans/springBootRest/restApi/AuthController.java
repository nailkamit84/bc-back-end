package com.asmajans.springBootRest.restApi;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.asmajans.springBootRest.Business.UserDetailsImpl;
import com.asmajans.springBootRest.DataAccess.IBolumDal;
import com.asmajans.springBootRest.DataAccess.UserRepository;
import com.asmajans.springBootRest.DataAccess.IUnvanlarDal;
import com.asmajans.springBootRest.Entities.ERole;
import com.asmajans.springBootRest.Entities.JwtResponse;
import com.asmajans.springBootRest.Entities.Kullanicilar;
import com.asmajans.springBootRest.Entities.LoginRequest;
import com.asmajans.springBootRest.Entities.MessageResponse;
import com.asmajans.springBootRest.Entities.SignupRequest;
import com.asmajans.springBootRest.Entities.Unvanlar;
import com.asmajans.springBootRest.Security.JwtUtils;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/auth")
public class AuthController {
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	IBolumDal bolumDal;

	@Autowired
	UserRepository userRepository;

	@Autowired
	IUnvanlarDal roleRepository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtUtils jwtUtils;

	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtUtils.generateJwtToken(authentication);

		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
		List<String> roles = userDetails.getAuthorities().stream().map(item -> item.getAuthority())
				.collect(Collectors.toList());

		return ResponseEntity.ok(new JwtResponse(jwt, userDetails.getId(), userDetails.getUsername(),
				userDetails.getEmail(), roles, userDetails.getAd_soyad()));
	}

	@PostMapping("/deleteUser")
	public ResponseEntity<?> deleteUser(@RequestBody Map<String,Object> body) {
		try {
			userRepository.setDurum((Integer.parseInt(body.get("id").toString())));
			return ResponseEntity.badRequest().body(new MessageResponse("Silme başarılı"));
		} catch (Exception e) {
			// TODO: handle exception
			return ResponseEntity.badRequest().body(new MessageResponse(e.getMessage()));
		}

	}

	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
		System.out.println(signUpRequest.getId());
		Kullanicilar user;
		System.out.println(signUpRequest.getId());
		if (userRepository.existsByUsername(signUpRequest.getUsername()) && signUpRequest.getId() == 0) {

			return ResponseEntity.badRequest().body(new MessageResponse("Error: Username is already taken!"));
		}

		if (userRepository.existsByEmail(signUpRequest.getEmail()) && signUpRequest.getId() == 0) {

			return ResponseEntity.badRequest().body(new MessageResponse("Error: Email is already in use!"));
		}
		if (signUpRequest.getId() == 0) {

			user = new Kullanicilar(signUpRequest.getUsername(), signUpRequest.getEmail(),
					encoder.encode(signUpRequest.getPassword()));
		} else {

			user = userRepository.getOne(signUpRequest.getId());
			user.setUsername(signUpRequest.getUsername());
			user.setPassword(encoder.encode(signUpRequest.getPassword()));
			user.setEmail(signUpRequest.getEmail());
			user.setId(signUpRequest.getId());
		}

		Set<String> strRoles = signUpRequest.getRole();
		Set<Unvanlar> roles = new HashSet<>();

		if (strRoles == null) {
			Unvanlar userRole = roleRepository.findByName(ERole.ROLE_OGRENCI)
					.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
			roles.add(userRole);
			user.setBolum(bolumDal.infoGetId(signUpRequest.getBolum().getId()));

		} else {
			strRoles.forEach(role -> {
				switch (role) {
				case "admin":
					Unvanlar adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(adminRole);

					break;
				case "grafiker":
					Unvanlar modRole = roleRepository.findByName(ERole.ROLE_GRAFIKER)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(modRole);

					break;
				default:
					Unvanlar userRole = roleRepository.findByName(ERole.ROLE_TEDARIKCI)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(userRole);
				}
			});
		}

		user.setUnvan(roles);
		user.setDogum_tarihi(signUpRequest.getDogum_tarihi());
		user.setAd_soyad(signUpRequest.getAd_soyad());

		user.setTelefon(signUpRequest.getTelefon());
		user.setAdres(signUpRequest.getAdres());
		Date date = new Date();
		user.setKayit_tarihi(date);
		userRepository.save(user);

		return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
	}
}