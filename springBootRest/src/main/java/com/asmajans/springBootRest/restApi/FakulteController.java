package com.asmajans.springBootRest.restApi;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.asmajans.springBootRest.Business.IFakulteService;
import com.asmajans.springBootRest.Entities.Fakulte;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api")
public class FakulteController {
	
	private IFakulteService fakulteService;
	
	@Autowired
	public FakulteController(IFakulteService fakulteService) {
		this.fakulteService = fakulteService;
	}
	
	@GetMapping("/fakulte/{id}")
	public List<Fakulte> getById(@PathVariable int id){
		return fakulteService.getById(id);
	}

}
