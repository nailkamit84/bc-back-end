package com.asmajans.springBootRest.restApi;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.asmajans.springBootRest.Business.IKampanyalarService;
import com.asmajans.springBootRest.Entities.AltOzellik;
import com.asmajans.springBootRest.Entities.Kampanyalar;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api")
public class KampanyalarController {

	private IKampanyalarService kampanyalarService;

	@Autowired
	public KampanyalarController(IKampanyalarService kampanyalarService) {
		this.kampanyalarService = kampanyalarService;
	}

	@GetMapping("/kampanyalar")
	public List<Kampanyalar> getAll() {
		return kampanyalarService.getAll();
	}
	
	@PostMapping("/addKampanya")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Kampanyalar add(@RequestBody Kampanyalar kampanya) {
		return kampanyalarService.add(kampanya);
		
	}

}
