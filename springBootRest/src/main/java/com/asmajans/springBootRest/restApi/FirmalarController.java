package com.asmajans.springBootRest.restApi;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.asmajans.springBootRest.Business.IFirmalarService;
import com.asmajans.springBootRest.Entities.Firmalar;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api")
public class FirmalarController {
	private IFirmalarService firmalarService;
	
	@Autowired
	public FirmalarController(IFirmalarService firmalarService) {
		this.firmalarService = firmalarService;
	}
	
	@GetMapping("/firmalar")
	public List<Firmalar> get(){
		return firmalarService.getAll();
	}
	
	@PostMapping("/addFirmalar")
	public Firmalar add(@RequestBody Firmalar firmalar) {
		return firmalarService.add(firmalar);
	}
	
	@DeleteMapping("/deleteFirmalar")
	public void delete(@RequestBody Firmalar firmalar) {
		 firmalarService.delete(firmalar);
	}
	
	@GetMapping("/firmalar/{id}")
	public List<Firmalar> getbyOgrenci(@PathVariable int id){
		return firmalarService.getbyOgrenci(id);
		
	}

}
