package com.asmajans.springBootRest.restApi;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.asmajans.springBootRest.Business.IUrunlerService;
import com.asmajans.springBootRest.Entities.Urunler;


@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api")
public class UrunlerController {

	private IUrunlerService urunlerService;
	
	@Autowired
	public UrunlerController(IUrunlerService urunlerService) {
		this.urunlerService = urunlerService;
	}
	
	@GetMapping("/urunler")
	public List<Urunler> get(){
		return urunlerService.getAll();
	}
	
	@PostMapping("/addUrun")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Urunler add(@RequestBody Urunler urun) {
		return urunlerService.add(urun);
	}
	
	@PostMapping("/updateUrun")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public void update(@RequestBody Urunler urun) {
		urunlerService.update(urun);
	}
	
	@PostMapping("/deleteUrun")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public void delete(@RequestBody Urunler urun) {
		urunlerService.delete(urun);
	}
	
	@GetMapping("/urunler/{id}")
	public Urunler getById(@PathVariable int id) {
		return urunlerService.getById(id);
		
	}
	
	@GetMapping("/urunler/yetki/{id}")
	public List<Urunler> getYetki(@PathVariable int id) {
		return urunlerService.getYetki(id);
		
	}
	
	@GetMapping("/urunler/yetkiBos")
	public List<Urunler> getYetkiBos() {
		return urunlerService.yetkiBos();
		
	}
	
	@PostMapping("/urunler/yetkiDuzenle")
	public void setYetkiduzenle(@RequestBody Map<String,Object> body){
		
		urunlerService.setYetkiDuzenle(body.get("urunler").toString(), Integer.parseInt(body.get("tedarikciId").toString()));
		//urunFiyatService.getbyAltOZellik(Integer.parseInt(body.get("urunId").toString()), body.get("altOzellik").toString());
		
	}
	
	@GetMapping("/urunler/kategori/{id}")
	public List<Urunler> getbyKategori(@PathVariable int id) {
		return urunlerService.getbyKategori(id);
		
	}

}
