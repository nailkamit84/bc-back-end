package com.asmajans.springBootRest.restApi;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.asmajans.springBootRest.Business.IUrunFiyatService;
import com.asmajans.springBootRest.Entities.UrunFiyat;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api")
public class UrunFiyatController {
	
	private IUrunFiyatService urunFiyatService;

	@Autowired
	public UrunFiyatController(IUrunFiyatService urunFiyatService) {
		this.urunFiyatService = urunFiyatService;
	}
	
	@GetMapping("/urunFiyat")
	public List<UrunFiyat> get(){
	
		return urunFiyatService.getAll();
	}
	
	@PostMapping("/addUrunFiyat")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public void add(@RequestBody UrunFiyat kategori) {
		urunFiyatService.add(kategori);
	}
	
	@PostMapping("/updateurunFiyat")
	public void update(@RequestBody UrunFiyat kategori) {
		urunFiyatService.update(kategori);
	}
	
	@PostMapping("/deleteurunFiyat")
	public void delete(@RequestBody UrunFiyat kategori) {
		urunFiyatService.delete(kategori);
	}
	
	@GetMapping("/urunFiyat/{id}")
	public UrunFiyat getById(@PathVariable int id) {
		return urunFiyatService.getById(id);
		
	}
	@GetMapping("/urunFiyat/urun/{id}")
	public List<UrunFiyat> getbyUrun(@PathVariable int id){
		return urunFiyatService.getbyUrun(id);
		
	}
	
	@PostMapping("/urunFiyat/altOzellik")
	public UrunFiyat getbyAltOzellik(@RequestBody Map<String,Object> body){
		//return urunFiyatService.getbyAltOZellik(Integer.parseInt(body.get("urunId").toString()), body.get("altOzellik").toString());
		return urunFiyatService.getbyAltOZellik(Integer.parseInt(body.get("urunId").toString()), body.get("altOzellik").toString());
		
	}

}
