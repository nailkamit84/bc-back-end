package com.asmajans.springBootRest.restApi;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.asmajans.springBootRest.Business.IOzelliklerService;
import com.asmajans.springBootRest.Entities.Ozellikler;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api")
public class OzelliklerController {
	
	private IOzelliklerService ozelliklerService;
	
	@Autowired
	public OzelliklerController(IOzelliklerService ozelliklerService) {
		this.ozelliklerService = ozelliklerService;
	}
	
	@GetMapping("/ozellikler")
	public List<Ozellikler> get(){
		return ozelliklerService.getAll();
	}
	
	@GetMapping("/ozellikler/{id}")
	public List<Ozellikler> getById(@PathVariable int id) {
		return ozelliklerService.getById(id);
		
	}
	@PostMapping("/addOzellikler")
	public Ozellikler add(@Valid @RequestBody Ozellikler ozellik) {
	
		return ozelliklerService.add(ozellik);
	}

}
