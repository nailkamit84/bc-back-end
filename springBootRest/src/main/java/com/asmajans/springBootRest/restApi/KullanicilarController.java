package com.asmajans.springBootRest.restApi;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.asmajans.springBootRest.Business.IKullanicilarService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api")
public class KullanicilarController {
	
private IKullanicilarService kullanicilarService;
	
	@Autowired
	public KullanicilarController(IKullanicilarService kullanicilarService) {
		this.kullanicilarService = kullanicilarService;
	}
	
	@GetMapping("/ogrenciler")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public List<Object[]> getOgrenciler() {
		return kullanicilarService.getOgrenciler();
		
	}
	
	@GetMapping("/tedarikciler")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public List<Object[]> getTedarikciler() {
		return kullanicilarService.getTedarikciler();
		
	}
	
	@GetMapping("/grafikerler")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public List<Object[]> getGrafikerler() {
		return kullanicilarService.getGrafikerler();
		
	}

}
