package com.asmajans.springBootRest.restApi;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.asmajans.springBootRest.Business.IBaskilarService;
import com.asmajans.springBootRest.Business.UploadManager;
import com.asmajans.springBootRest.Entities.Baskilar;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api")
public class BaskilarController {
	
	@Autowired
	private UploadManager storageService;
	
	private IBaskilarService baskilarService;

	@Autowired
	public BaskilarController(IBaskilarService baskilarService) {
		this.baskilarService = baskilarService;
	}
	
	@GetMapping("/baskilar")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public List<Baskilar> get(){
		return baskilarService.getAll();
	}
	
	@PostMapping("/addBaski")
	public Baskilar add(@RequestBody Baskilar baski) {
		return baskilarService.add(baski);
	}
	
	@PostMapping("/updateBaski")
	@PreAuthorize("hasRole('ROLE_GRAFIKER')")
	public void update(@RequestBody Map<String,Object> body) {
		baskilarService.update(body.get("tasarim_url").toString(), Integer.parseInt(body.get("id").toString()));
	}

	@GetMapping("/sepet/{id}")
	@PreAuthorize("hasRole('ROLE_OGRENCI')")
	public List<Baskilar> getById(@PathVariable int id) {
		return baskilarService.getSepet(id);
	}
	
	@GetMapping("/sepettenCikar/{id}")
	@PreAuthorize("hasRole('ROLE_OGRENCI')")
	public void listedenCikar(@PathVariable int id) {
		Baskilar baski=baskilarService.getById(id);
		storageService.deleteFile("images/tasarim/"+baski.getTasarim_url());
		String[] dizi=baski.getGorseller_url().split(",");
		for (int i = 0; i < dizi.length; i++) {
			storageService.deleteFile("images/gorseller/"+dizi[i]);
		}
		baskilarService.listedenCikar(id);
	}
	
	@PostMapping("/sepetOnay")
	@PreAuthorize("hasRole('ROLE_OGRENCI')")
	public void sepetOnay(@RequestBody Baskilar baski) {
		baskilarService.sepetOnay(baski);
	}
	
	@GetMapping("/tasarimAl/{id}")
	@PreAuthorize("hasRole('ROLE_GRAFIKER')")
	public List<Baskilar> tasarimAl(@PathVariable int id) {
		return baskilarService.tasarimAl(id);
	}
	
	@PostMapping("/baskilar/filtre")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public List<Baskilar> getBaskilarFiltre(@RequestBody Map<String,Object> body) throws ParseException {

		return baskilarService.getBaskilarFiltre(body.get("kategoriId").toString(), body.get("urunId").toString(), body.get("firmaId").toString(), body.get("ogrenciId").toString(), body.get("grafikerId").toString(), body.get("tedarikciId").toString(), body.get("siparis_baslangic").toString(), body.get("siparis_bitis").toString());
		
	}
	
	@PostMapping("/baskilar/filtre/ogrenci")
	@PreAuthorize("hasRole('ROLE_OGRENCI')")
	public List<Baskilar> getBaskilarFiltreOgrenci(@RequestBody Map<String,Object> body) throws ParseException {

		return baskilarService.getBakilarFiltreOgrenci(body.get("kategori").toString(), body.get("urun").toString(), body.get("firma").toString(), body.get("siparis_baslangic").toString(), body.get("siparis_bitis").toString(), Integer.parseInt(body.get("ogrenciId").toString()));
	}
	
	@PostMapping("/baskilar/filtre/ogrenci/siparisler")
	@PreAuthorize("hasRole('ROLE_OGRENCI')")
	public List<Baskilar> getBaskilarFiltreOgrenciSiparisler(@RequestBody Map<String,Object> body) throws ParseException {

		return baskilarService.getBakilarFiltreOgrenciSiparisler(body.get("kategori").toString(), body.get("urun").toString(), body.get("firma").toString(), body.get("siparis_baslangic").toString(), body.get("siparis_bitis").toString(), Integer.parseInt(body.get("ogrenciId").toString()));
	}
	
	@PostMapping("/baskilar/filtre/grafiker")
	@PreAuthorize("hasRole('ROLE_GRAFIKER')")
	public List<Baskilar> getBaskilarFiltreGrafiker(@RequestBody Map<String,Object> body) throws ParseException {

		return baskilarService.getBakilarFiltreGrafiker(body.get("kategori").toString(), body.get("urun").toString(), body.get("firma").toString(), body.get("siparis_baslangic").toString(), body.get("siparis_bitis").toString(), Integer.parseInt(body.get("grafikerId").toString()));
	}
	
	@PostMapping("/baskilar/filtre/tedarikci")
	@PreAuthorize("hasRole('ROLE_TEDARIKCI')")
	public List<Baskilar> getBaskilarFiltreTedarikci(@RequestBody Map<String,Object> body) throws ParseException {

		return baskilarService.getBakilarFiltreTedarikci(body.get("kategori").toString(), body.get("urun").toString(), body.get("firma").toString(), body.get("siparis_baslangic").toString(), body.get("siparis_bitis").toString(), Integer.parseInt(body.get("tedarikciId").toString()));
	}
	
	@GetMapping("/tasarimOnay/{id}")
	@PreAuthorize("hasRole('ROLE_GRAFIKER')")
	public void tasarimOnay(@PathVariable int id) {
		baskilarService.setOnay(id);
	}
	
	@PostMapping("/revize")
	@PreAuthorize("hasRole('ROLE_OGRENCI')")
	public void revize(@RequestBody Map<String,Object> body) {
		baskilarService.revize(Integer.parseInt(body.get("id").toString()), body.get("revizeNotu").toString());
	}
	
	@GetMapping("/ogrenciOnay/{id}")
	@PreAuthorize("hasRole('ROLE_OGRENCI')")
	public void ogrenciOnay(@PathVariable int id) {
		baskilarService.ogrenciOnay(id);
	}
	
	@GetMapping("/tedarikciList/{id}")
	@PreAuthorize("hasRole('ROLE_TEDARIKCI')")
	public List<Baskilar> tedarikciList(@PathVariable int id) {
		return baskilarService.tedarikciList(id);
	}
	
	@PostMapping("/updateDurum")
	@PreAuthorize("hasRole('ROLE_TEDARIKCI')")
	public void updateDurum(@RequestBody Map<String,Object> body) {
		baskilarService.updateDurum(Integer.parseInt(body.get("siparis_durumu").toString()), Integer.parseInt(body.get("kargo_firmasi_id").toString()), body.get("kargo_takip_no").toString(), Integer.parseInt(body.get("id").toString()));
	}
	
	@GetMapping("/baskilar/{id}")
	@PreAuthorize("hasRole('ROLE_OGRENCI')")
	public List<Baskilar> getOgrenciBaski(@PathVariable int id){
		return baskilarService.getOgrenciBaski(id);
	}
	
	@GetMapping("/baskilar/grafiker/{id}")
	@PreAuthorize("hasRole('ROLE_GRAFIKER')")
	public List<Baskilar> getGrafikerBaski(@PathVariable int id){
		return baskilarService.getGrafikerBaski(id);
	}
	
	@GetMapping("/baskilar/tedarikci/{id}")
	@PreAuthorize("hasRole('ROLE_TEDARIKCI')")
	public List<Baskilar> getTedarikciBaski(@PathVariable int id){
		return baskilarService.getTedarikciBaski(id);
	}
	
	@GetMapping("/baskilar/teslim/{id}")
	@PreAuthorize("hasRole('ROLE_OGRENCI')")
	public void setTeslim(@PathVariable int id){
		baskilarService.setTeslim(id);
	}
	
	
}
