package com.asmajans.springBootRest.restApi;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.asmajans.springBootRest.Business.IKargoFirmalariService;
import com.asmajans.springBootRest.Entities.KargoFirmalari;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api")
public class KargoFirmalariController {
	
	private IKargoFirmalariService kargoFirmalariService;

	@Autowired
	public KargoFirmalariController(IKargoFirmalariService kargoFirmalariService) {
		this.kargoFirmalariService = kargoFirmalariService;
	}
	
	@GetMapping("/kargoFirmalari")
	@PreAuthorize("hasRole('ROLE_TEDARIKCI')")
	public List<KargoFirmalari> get(){
		return kargoFirmalariService.getAll();
	}

}
