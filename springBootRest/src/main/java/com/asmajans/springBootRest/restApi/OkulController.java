package com.asmajans.springBootRest.restApi;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.asmajans.springBootRest.Business.IOkulService;
import com.asmajans.springBootRest.Entities.Okul;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api")
public class OkulController {
	
	private IOkulService okulService;
	
	@Autowired
	public OkulController(IOkulService okulService) {
		this.okulService = okulService;
	}
	
	@GetMapping("/okullar")
	public List<Okul> get(){
		return okulService.getAll();
	}
	
	@PostMapping("/addOkul")
	public Okul add(@RequestBody Okul okul) {
		return okulService.add(okul);
	}
	
	@PostMapping("/updateOkul")
	public Okul update(@RequestBody Okul okul) {
		return okulService.update(okul);
	}
	
	@PostMapping("/deleteOkul")
	public Okul delete(@RequestBody Okul okul) {
		return okulService.delete(okul);
	}
	
	@GetMapping("/okullar/{id}")
	public Okul getById(@PathVariable int id) {
		return okulService.getById(id);
		
	}

}
