package com.asmajans.springBootRest.restApi;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.asmajans.springBootRest.Business.IAltOzellikService;
import com.asmajans.springBootRest.Entities.AltOzellik;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api")
public class AltOzellikController {
	
	private IAltOzellikService altOzellikService;

	@Autowired
	public AltOzellikController(IAltOzellikService altOzellikService) {
		this.altOzellikService = altOzellikService;
	}
	
	@GetMapping("/altOzellikler")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public List<AltOzellik> get(){
		return altOzellikService.getAll();
	}
	
	@PostMapping("/addAltOzellik")
	public void add(@RequestBody AltOzellik altOzellik) {
		altOzellikService.add(altOzellik);
	}
	
	@PostMapping("/updateAltOzellik")
	public void update(@RequestBody AltOzellik altOzellik) {
		altOzellikService.update(altOzellik);
	}
	
	@PostMapping("/deleteAltOzellik")
	public void delete(@RequestBody AltOzellik altOzellik) {
		altOzellikService.delete(altOzellik);
	}
	
	@GetMapping("/altOzellikler/{id}")
	public List<AltOzellik> getById(@PathVariable int id) {
		return altOzellikService.getById(id);
		
	}
	@GetMapping("/altOzellikler/urun/{id}")
	public List<AltOzellik> getByUrun(@PathVariable int id) {
		return altOzellikService.getByUrun(id);
		
	}

}
