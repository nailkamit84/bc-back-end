package com.asmajans.springBootRest.restApi;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.asmajans.springBootRest.Entities.*;
import com.asmajans.springBootRest.Business.IKategorilerService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api")
public class KategorilerController {
	private IKategorilerService kategorilerService;

	@Autowired
	public KategorilerController(IKategorilerService kategorilerService) {
		this.kategorilerService = kategorilerService;
	}
	
	@GetMapping("/kategoriler")
	public List<Kategoriler> get(){
	
		return kategorilerService.getAll();
	}
	
	@PostMapping("/addKategori")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public void add(@RequestBody Kategoriler kategori) {
		kategorilerService.add(kategori);
	}
	
	@PostMapping("/updateKategori")
	public void update(@RequestBody Kategoriler kategori) {
		kategorilerService.update(kategori);
	}
	
	@PostMapping("/deleteKategori")
	public void delete(@RequestBody Kategoriler kategori) {
		kategorilerService.delete(kategori);
	}
	
	@GetMapping("/kategoriler/{id}")
	public Kategoriler getById(@PathVariable int id) {
		return kategorilerService.getById(id);
		
	}

}
