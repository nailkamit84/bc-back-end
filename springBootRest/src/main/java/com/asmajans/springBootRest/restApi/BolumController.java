package com.asmajans.springBootRest.restApi;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.asmajans.springBootRest.Business.IBolumService;
import com.asmajans.springBootRest.Entities.Bolum;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api")
public class BolumController {
	
	private IBolumService bolumService;
	
	@Autowired
	public BolumController(IBolumService bolumService) {
		this.bolumService = bolumService;
	}
	
	@GetMapping("/bolumler/{id}")
	public List<Bolum> getById(@PathVariable int id) {
		return bolumService.getById(id);
		
	}

}
