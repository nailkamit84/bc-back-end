package com.asmajans.springBootRest.DataAccess;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.asmajans.springBootRest.Entities.Kategoriler;

@Repository
public class HibernateKategorilerDal implements IKategoriDal {

	private EntityManager entityManager;
	
	@Autowired
	public HibernateKategorilerDal(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	@Override
	@Transactional
	public List<Kategoriler> getAll() {
		Session session = entityManager.unwrap(Session.class);
		List<Kategoriler> kategoriler = session.createQuery("FROM Kategoriler", Kategoriler.class).getResultList();
		return kategoriler;
	}

	@Override
	@Transactional
	public void add(Kategoriler kategori) {
		Session session = entityManager.unwrap(Session.class);
		session.saveOrUpdate(kategori);
		
		
	}

	@Override
	public void update(Kategoriler kategori) {
		Session session = entityManager.unwrap(Session.class);
		session.saveOrUpdate(kategori);
		
	}

	@Override
	public void delete(Kategoriler kategori) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Kategoriler getById(int id) {
		Session session = entityManager.unwrap(Session.class);
		Kategoriler kategori = session.get(Kategoriler.class, id);
		return kategori;
	}

}
