package com.asmajans.springBootRest.DataAccess;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.asmajans.springBootRest.Entities.Kampanyalar;

@Repository
public class HibernateKampanyalarDal implements IKampanyalarDal {

	private EntityManager entityManager;

	@Autowired
	public HibernateKampanyalarDal(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Override
	@Transactional
	public List<Kampanyalar> getAll() {
		Session session = entityManager.unwrap(Session.class);
		List<Kampanyalar> kampanyalar = session.createQuery("FROM Kampanyalar", Kampanyalar.class).getResultList();
		return kampanyalar;
		
		
	}

	@Override
	@Transactional
	public Kampanyalar add(Kampanyalar kampanya) {
		Session session = entityManager.unwrap(Session.class);
		Query query = session.createQuery("UPDATE Kampanyalar SET slide_url='"+kampanya.getSlide_url()+"', sirasi="+kampanya.getSirasi()+"  WHERE id=" + kampanya.getId());
		query.executeUpdate();
		return kampanya;
	}

}
