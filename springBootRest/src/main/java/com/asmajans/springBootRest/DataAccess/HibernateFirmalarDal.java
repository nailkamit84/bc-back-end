package com.asmajans.springBootRest.DataAccess;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.asmajans.springBootRest.Entities.Firmalar;

@Repository
public class HibernateFirmalarDal implements IFirmalarDal {

	private EntityManager entityManager;
	
	@Autowired
	public HibernateFirmalarDal(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	@Override
	@Transactional
	public List<Firmalar> getAll() {
		Session session = entityManager.unwrap(Session.class);
		List<Firmalar> firmalar = session.createQuery("FROM Firmalar", Firmalar.class).getResultList();
		return firmalar;
	}

	@Override
	@Transactional
	public Firmalar add(Firmalar firmalar) {
		Session session = entityManager.unwrap(Session.class);
		session.saveOrUpdate(firmalar);
		return firmalar;
		
	}

	@Override
	public void update(Firmalar firmalar) {
		// TODO Auto-generated method stub
		
	}

	@Override
	@Transactional
	public void delete(Firmalar firmalar) {
		Session session = entityManager.unwrap(Session.class);
		session.delete(firmalar);
		
	}

	@Override
	public List<Firmalar> getById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Firmalar infoId(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Firmalar> getbyOgrenci(int id) {
		Session session = entityManager.unwrap(Session.class);
		List<Firmalar> firmalar = session.createQuery("FROM Firmalar WHERE ogrenci.id="+id, Firmalar.class).getResultList();
		return firmalar;
	}

}
