package com.asmajans.springBootRest.DataAccess;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.asmajans.springBootRest.Entities.Bolum;


@Repository
public class HibernateBolumDal implements IBolumDal {
	
	private EntityManager entityManager;
	
	@Autowired
	public HibernateBolumDal(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Override
	@Transactional
	public List<Bolum> getById(int id) {
		Session session = entityManager.unwrap(Session.class);
		List<Bolum>  bolumler = session.createQuery("FROM Bolum Where fakulte_id=:id",Bolum.class).setParameter("id", id).getResultList();
		return bolumler;
	}

	@Override
	public Bolum infoGetId(int id) {
		Session session = entityManager.unwrap(Session.class);
		Bolum bolum = session.get(Bolum.class, id);
		return bolum;
	}

}
