package com.asmajans.springBootRest.DataAccess;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.asmajans.springBootRest.Entities.Kullanicilar;

@Repository
public interface UserRepository extends JpaRepository<Kullanicilar, Integer> {
	
	Optional<Kullanicilar> findByUsername(String username);

	Boolean existsByUsername(String username);

	Boolean existsByEmail(String email);
	
	@Query("UPDATE FROM Kullanicilar k SET k.durum=false WHERE id = ?1 ")
	void setDurum(int id);

}
