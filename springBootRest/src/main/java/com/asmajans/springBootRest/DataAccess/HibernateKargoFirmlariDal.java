package com.asmajans.springBootRest.DataAccess;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.asmajans.springBootRest.Entities.KargoFirmalari;

@Repository
public class HibernateKargoFirmlariDal implements IKargoFirmalariDal {

	
	private EntityManager entityManager;

	@Autowired
	public HibernateKargoFirmlariDal(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	@Override
	@Transactional
	public List<KargoFirmalari> getAll() {
		Session session = entityManager.unwrap(Session.class);
		List<KargoFirmalari> kargoFirmalari = session.createQuery("FROM KargoFirmalari", KargoFirmalari.class).getResultList();
		return kargoFirmalari;
	}

}
