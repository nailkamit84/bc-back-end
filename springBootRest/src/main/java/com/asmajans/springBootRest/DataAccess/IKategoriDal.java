package com.asmajans.springBootRest.DataAccess;
import java.util.List;


import com.asmajans.springBootRest.Entities.Kategoriler;


public interface IKategoriDal {

	List<Kategoriler> getAll();
	void add(Kategoriler kategori);
	void update(Kategoriler kategori);
	void delete(Kategoriler kategori);
	Kategoriler getById(int id);
}
