package com.asmajans.springBootRest.DataAccess;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.asmajans.springBootRest.Entities.AltOzellik;

@Repository
public class HibernateAltOzellikDal implements IAltOzellikDal {
	
	private EntityManager entityManager;

	@Autowired
	public HibernateAltOzellikDal(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Override
	@Transactional
	public List<AltOzellik> getAll() {
		Session session = entityManager.unwrap(Session.class);
		List<AltOzellik> altOzellik = session.createQuery("FROM AltOzellik", AltOzellik.class).getResultList();
		return altOzellik;
	}

	@Override
	public void add(AltOzellik altOzellik) {
		Session session = entityManager.unwrap(Session.class);
		session.saveOrUpdate(altOzellik);
		
	}

	@Override
	public void update(AltOzellik altOzellik) {
		Session session = entityManager.unwrap(Session.class);
		session.saveOrUpdate(altOzellik);
		
	}

	@Override
	public void delete(AltOzellik altOzellik) {
		// TODO Auto-generated method stub
		
	}

	@Override
	@Transactional
	public List<AltOzellik> getById(int id) {
		Session session = entityManager.unwrap(Session.class);
		List<AltOzellik> altOzellikler = session.createQuery("FROM AltOzellik o WHERE o.ozellik="+id, AltOzellik.class).getResultList();
		return altOzellikler;
	}

	@Override
	public AltOzellik infoId(int id) {
		Session session = entityManager.unwrap(Session.class);
		AltOzellik altOzellik = session.get(AltOzellik.class, id);
		return altOzellik;
		
	}

	@Override
	public List<AltOzellik> getByUrun(int id) {
		Session session = entityManager.unwrap(Session.class);
		List<AltOzellik> altOzellikler = session.createQuery("FROM AltOzellik o WHERE o.ozellik.urun.id="+id, AltOzellik.class).getResultList();
		return altOzellikler;
	}

}
