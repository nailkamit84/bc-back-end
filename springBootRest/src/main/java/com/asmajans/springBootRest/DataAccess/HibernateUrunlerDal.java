package com.asmajans.springBootRest.DataAccess;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.asmajans.springBootRest.Entities.Urunler;

@Repository
public class HibernateUrunlerDal implements IUrunlerDal {

	private EntityManager entityManager;

	@Autowired
	public HibernateUrunlerDal(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Override
	@Transactional
	public List<Urunler> getAll() {
		Session session = entityManager.unwrap(Session.class);
		List<Urunler> urun = session.createQuery("FROM Urunler", Urunler.class).getResultList();
		return urun;
	}

	@Override
	public Urunler add(Urunler urun) {
		Session session = entityManager.unwrap(Session.class);
		session.saveOrUpdate(urun);
		return urun;

	}

	@Override
	public void update(Urunler urun) {
		Session session = entityManager.unwrap(Session.class);
		session.saveOrUpdate(urun);

	}

	@Override
	public void delete(Urunler urun) {
		Session session = entityManager.unwrap(Session.class);
		Urunler urunToDelete = session.get(Urunler.class, urun.getId());
		session.delete(urunToDelete);

	}

	@Override
	public Urunler getById(int id) {
		Session session = entityManager.unwrap(Session.class);
		Urunler urun = session.get(Urunler.class, id);
		return urun;
	}

	@Override
	public List<Urunler> getYetki(int id) {
		Session session = entityManager.unwrap(Session.class);
		List<Urunler> urun = session.createQuery("FROM Urunler WHERE basim_yeri=" + id, Urunler.class).getResultList();
		return urun;
	}

	@Override
	public List<Urunler> yetkiBos() {
		Session session = entityManager.unwrap(Session.class);
		List<Urunler> urun = session.createQuery("FROM Urunler WHERE basim_yeri=null", Urunler.class).getResultList();
		return urun;
	}

	@Override
	@Transactional
	public void setYetkiDuzenle(String urunler, int tedarikciId) {
		Session session = entityManager.unwrap(Session.class);
		Query query2 = session.createQuery("UPDATE Urunler SET basim_yeri=null where basim_yeri=" + tedarikciId);
		query2.executeUpdate();
		if (urunler != "") {
			String[] dizi2 = urunler.split(",");
			for (String urun : dizi2) {
				Query query = session
						.createQuery("UPDATE Urunler SET basim_yeri=" + tedarikciId + " where id=" + urun);
				query.executeUpdate();
			}
		}



	}

	@Override
	public List<Urunler> getbyKategori(int id) {
		Session session = entityManager.unwrap(Session.class);
		List<Urunler> urun = session.createQuery("FROM Urunler WHERE kategori="+id, Urunler.class).getResultList();
		return urun;
	}

}
