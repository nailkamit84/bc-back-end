package com.asmajans.springBootRest.DataAccess;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.asmajans.springBootRest.Entities.UrunFiyat;

@Repository
public class HibernateUrunFiyatDal implements IUrunFiyatDal {

private EntityManager entityManager;
	
	@Autowired
	public HibernateUrunFiyatDal(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	@Override
	@Transactional
	public List<UrunFiyat> getAll() {
		Session session = entityManager.unwrap(Session.class);
		List<UrunFiyat> urunFiyat = session.createQuery("FROM UrunFiyat", UrunFiyat.class).getResultList();
		return urunFiyat;
	}

	@Override
	public void add(UrunFiyat urunFiyat) {
		Session session = entityManager.unwrap(Session.class);
		session.saveOrUpdate(urunFiyat);
		
	}

	@Override
	public void update(UrunFiyat urunFiyat) {
		Session session = entityManager.unwrap(Session.class);
		session.saveOrUpdate(urunFiyat);
		
	}

	@Override
	public void delete(UrunFiyat urunFiyat) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public UrunFiyat getById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional
	public List<UrunFiyat> getbyUrun(int id) {
		Session session = entityManager.unwrap(Session.class);
		List<UrunFiyat> urunFiyat = session.createQuery("FROM UrunFiyat WHERE urun.id="+id, UrunFiyat.class).getResultList();
		return urunFiyat;
	}

	@Override
	public UrunFiyat getbyAltOZellik(int id, String AltOzellik) {
		Session session = entityManager.unwrap(Session.class);
		UrunFiyat urunFiyat = session.createQuery("FROM UrunFiyat WHERE urun.id="+id+" AND alt_ozellikler='"+AltOzellik+"'", UrunFiyat.class).getSingleResult();
		return urunFiyat;
	}

}
