package com.asmajans.springBootRest.DataAccess;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.asmajans.springBootRest.Entities.ERole;
import com.asmajans.springBootRest.Entities.Unvanlar;

@Repository
public interface IUnvanlarDal extends JpaRepository<Unvanlar, Integer> {

	Optional<Unvanlar> findByName(ERole name);
}
