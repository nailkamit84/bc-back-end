package com.asmajans.springBootRest.DataAccess;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.asmajans.springBootRest.Entities.Ozellikler;

@Repository
public class HibernateOzeliklerDal implements IOzelliklerDal {

	private EntityManager entityManager;
	
	@Autowired
	public HibernateOzeliklerDal(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Override
	@Transactional
	public List<Ozellikler> getAll() {
		Session session = entityManager.unwrap(Session.class);
		List<Ozellikler> ozellik = session.createQuery("FROM Ozellikler", Ozellikler.class).getResultList();
		return ozellik;
	}

	@Override
	public Ozellikler add(Ozellikler ozellik) {
		Session session = entityManager.unwrap(Session.class);
		session.saveOrUpdate(ozellik);
		return ozellik;
	}

	@Override
	public void update(Ozellikler ozellik) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Ozellikler ozellik) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Ozellikler> getById(int id) {
		Session session = entityManager.unwrap(Session.class);
		List<Ozellikler> ozellik = session.createQuery("FROM Ozellikler o WHERE o.urun="+id, Ozellikler.class).getResultList();
		return ozellik;
	}

	@Override
	public Ozellikler infoId(int id) {
		Session session = entityManager.unwrap(Session.class);
		Ozellikler ozellikler = session.get(Ozellikler.class, id);
		return ozellikler;
	}
}
