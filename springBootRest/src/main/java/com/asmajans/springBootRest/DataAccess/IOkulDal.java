package com.asmajans.springBootRest.DataAccess;

import java.util.List;

import com.asmajans.springBootRest.Entities.Okul;

public interface IOkulDal {

	List<Okul> getAll();
	Okul add(Okul okul);
	Okul update(Okul okul);
	Okul delete(Okul okul);
	Okul getById(int id);
}
