package com.asmajans.springBootRest.DataAccess;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.asmajans.springBootRest.Entities.Kullanicilar;


@Repository
public class HibernateKullanicilar implements IKullanicilarDal {

	private EntityManager entityManager;
	
	@Autowired
	public HibernateKullanicilar(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	@Override
	@Transactional
	public List<Object[]> getOgrenciler() {
		Session session = entityManager.unwrap(Session.class);
		List<Object[]>  ogrenciler = session.createQuery("select k, (select count(b) from Baskilar b where b.ogrenci=k.id) FROM Kullanicilar k join fetch k.unvan u WHERE u.name='ROLE_OGRENCI'",Object[].class).getResultList();
		return ogrenciler;
	}

	@Override
	@Transactional
	public List<Object[]> getTedarikciler() {
		Session session = entityManager.unwrap(Session.class);
		List<Object[]>  tedarikciler = session.createQuery("select k, (select count(b) from Baskilar b where b.urun_fiyat.urun.basim_yeri=k.id) FROM Kullanicilar k join fetch k.unvan u WHERE u.name='ROLE_TEDARIKCI'",Object[].class).getResultList();
		return tedarikciler;
	}

	@Override
	@Transactional
	public List<Object[]> getGrafikerler() {
		Session session = entityManager.unwrap(Session.class);
		List<Object[]>  grafikerler = session.createQuery("select k, (select count(b) from Baskilar b where b.grafiker=k.id) FROM Kullanicilar k join fetch k.unvan u WHERE u.name='ROLE_GRAFIKER'",Object[].class).getResultList();
		return grafikerler;
	}

	@Override
	public Kullanicilar getById(int id) {
		Session session = entityManager.unwrap(Session.class);
		Kullanicilar kullanicilar = session.get(Kullanicilar.class, id);
		return kullanicilar;
	}

}
