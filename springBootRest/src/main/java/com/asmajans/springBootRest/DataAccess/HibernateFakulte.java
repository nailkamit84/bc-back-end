package com.asmajans.springBootRest.DataAccess;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.asmajans.springBootRest.Entities.Fakulte;

@Repository
public class HibernateFakulte implements IFakulteDal {

	private EntityManager entityManager;
	
	@Autowired
	public HibernateFakulte(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Override
	@Transactional
	public List<Fakulte> getById(int id) {
		Session session = entityManager.unwrap(Session.class);
		List<Fakulte>  fakulteler = session.createQuery("FROM Fakulte Where okul_id=:id",Fakulte.class).setParameter("id", id).getResultList();
		return fakulteler;
	}

}
