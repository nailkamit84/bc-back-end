package com.asmajans.springBootRest.DataAccess;

import java.util.List;

import com.asmajans.springBootRest.Entities.KargoFirmalari;


public interface IKargoFirmalariDal {
	
	List<KargoFirmalari> getAll();

}
