package com.asmajans.springBootRest.DataAccess;

import java.util.List;

import com.asmajans.springBootRest.Entities.Bolum;

public interface IBolumDal {
	
	List<Bolum> getById(int id);
	Bolum infoGetId(int id);

}
