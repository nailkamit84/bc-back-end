package com.asmajans.springBootRest.DataAccess;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.asmajans.springBootRest.Entities.Baskilar;

@Repository
public class HibernateBaskilarDal implements IBaskilarDal {

	private EntityManager entityManager;

	@Autowired
	public HibernateBaskilarDal(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Override
	@Transactional
	public List<Baskilar> getAll() {
		Session session = entityManager.unwrap(Session.class);
		List<Baskilar> baskilar = session.createQuery("FROM Baskilar", Baskilar.class).getResultList();
		return baskilar;
	}

	@Override
	public Baskilar add(Baskilar baski) {
		Session session = entityManager.unwrap(Session.class);
		session.saveOrUpdate(baski);
		return baski;

	}

	@Override
	@Transactional
	public void update(String url, int id) {
		Session session = entityManager.unwrap(Session.class);
		Query query = session.createQuery("UPDATE Baskilar SET siparis_durumu=3, tasarim_url='" + url + "' WHERE id=" + id);
		query.executeUpdate();
	}

	@Override
	public Baskilar delete(Baskilar baski) {
		// TODO Auto-generated method stub
		return null;

	}

	@Override
	public Baskilar getById(int id) {
		Session session = entityManager.unwrap(Session.class);
		Baskilar baski = session.get(Baskilar.class, id);
		return baski;

	}

	@Override
	@Transactional
	public List<Baskilar> getSepet(int id) {
		Session session = entityManager.unwrap(Session.class);
		List<Baskilar> baskilar = session
				.createQuery("FROM Baskilar WHERE sepette_mi=true AND ogrenci.id=" + id, Baskilar.class)
				.getResultList();
		return baskilar;
	}

	@Override
	@Transactional
	public void listedenCikar(int id) {
		Session session = entityManager.unwrap(Session.class);
		Query query = session.createQuery("DELETE Baskilar WHERE id=" + id);
		query.executeUpdate();
	}

	@Override
	@Transactional
	public void sepetOnay(Baskilar baski) {
		Session session = entityManager.unwrap(Session.class);
		Query query = session
				.createQuery("UPDATE Baskilar SET sepette_mi=false, siparis_durumu=1, kayit_tarihi=NOW(), siparisKodu="
						+ baski.getSiparisKodu() + " WHERE sepette_mi=true AND ogrenci.id="
						+ baski.getOgrenci().getId());
		query.executeUpdate();
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<Baskilar> tasarimAl(int id) {
		Session session = entityManager.unwrap(Session.class);
		Query query = session.createQuery("FROM Baskilar b WHERE (b.siparis_durumu=2 OR b.siparis_durumu=5) AND grafiker.id=" + id);
		System.out.println("girdi");
		List<Baskilar> oncekiTasarim = query.getResultList();
		Baskilar siparisKodu;
		if (oncekiTasarim.isEmpty()) {
			System.out.println("girdiiii");
			List<Baskilar> siparisKodu2 = session
					.createQuery("FROM Baskilar b WHERE b.siparis_durumu=1 ORDER BY b.kayit_tarihi desc",
							Baskilar.class)
					.getResultList();
			if (!siparisKodu2.isEmpty()) {
				siparisKodu = siparisKodu2.get(0);

				
				List<Baskilar> baskilar = session.createQuery(
						"FROM Baskilar WHERE siparis_durumu=1  AND siparisKodu='" + siparisKodu.getSiparisKodu() + "'",
						Baskilar.class).getResultList();
				Query query2 = session.createQuery("UPDATE Baskilar SET siparis_durumu=2, grafiker.id=" + id
						+ ", grafikere_gidis_tarihi=NOW() WHERE siparis_durumu=1  AND siparisKodu='"
						+ siparisKodu.getSiparisKodu() + "'");
				query2.executeUpdate();
				

				return baskilar;
			} else {
				return null;
			}
		} else {
			return oncekiTasarim;
		}
	}

	@Override
	@Transactional
	public List<Baskilar> getBaskilarFiltre(String kategoriId, String urunId, String firmaId, String ogrenciId,
			String grafikerId, String tedarikciId, String siparis_baslangic, String siparis_bitis) {

		//try {
			System.out.println("geldi");
			Session session = entityManager.unwrap(Session.class);
			//SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			String sql;
			if (siparis_baslangic != "" && siparis_bitis != "") {
				//Date siparis_baslangicc = formatter.parse(siparis_baslangic.toString());
				//Date siparis_bitiss = formatter.parse(siparis_bitis.toString());
				sql = "FROM Baskilar WHERE urun_fiyat.urun.kategori.kategori_adi LIKE '%" + kategoriId
						+ "%' AND urun_fiyat.urun.urun_adi LIKE '%" + urunId + "%' AND firma.firma_adi LIKE '%"
						+ firmaId + "%' AND ogrenci.ad_soyad LIKE '%" + ogrenciId + "%' AND grafiker.ad_soyad LIKE '%"
						+ grafikerId + "%' AND kayit_tarihi BETWEEN '" + siparis_baslangic + "' AND '" + siparis_bitis
						+ " 23:59:59.999999'";
			} else {
				sql = "FROM Baskilar WHERE urun_fiyat.urun.kategori.kategori_adi LIKE '%" + kategoriId
						+ "%' AND urun_fiyat.urun.urun_adi LIKE '%" + urunId + "%' AND firma.firma_adi LIKE '%"
						+ firmaId + "%' AND ogrenci.ad_soyad LIKE '%" + ogrenciId + "%' AND grafiker.ad_soyad LIKE '%"
						+ grafikerId + "%' ";
			}

			System.out.println(sql);
			List<Baskilar> baskilar = session.createQuery(sql, Baskilar.class).getResultList();
			return baskilar;
		//} catch (ParseException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			//return null;
		//}

	}

	@Override
	@Transactional
	public List<Baskilar> getBakilarFiltreOgrenci(String kategori, String urun, String firma, String siparis_baslangic,
			String siparis_bitis, int ogrenciId) {

		//try {
			Session session = entityManager.unwrap(Session.class);
			//SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			String sql;
			if (siparis_baslangic != "" && siparis_bitis != "") {
				//Date siparis_baslangicc = formatter.parse(siparis_baslangic.toString());
				//Date siparis_bitiss = formatter.parse(siparis_bitis.toString());
				sql = "FROM Baskilar WHERE ogrenci=" + ogrenciId
						+ " AND siparis_durumu=8 AND urun_fiyat.urun.kategori.kategori_adi LIKE '%" + kategori
						+ "%' AND urun_fiyat.urun.urun_adi LIKE '%" + urun + "%' AND firma.firma_adi LIKE '%" + firma
						+ "%' AND kayit_tarihi BETWEEN '" + siparis_baslangic + "' AND '" + siparis_bitis + " 23:59:59.999999'";
			} else {
				sql = "FROM Baskilar WHERE ogrenci=" + ogrenciId
						+ " AND siparis_durumu=8 AND urun_fiyat.urun.kategori.kategori_adi LIKE '%" + kategori
						+ "%' AND urun_fiyat.urun.urun_adi LIKE '%" + urun + "%' AND firma.firma_adi LIKE '%" + firma
						+ "%' ";
			}

			List<Baskilar> baskilar = session.createQuery(sql, Baskilar.class).getResultList();
			return baskilar;
		//} catch (ParseException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			//return null;
		//}
	}

	@Override
	@Transactional
	public List<Baskilar> getBakilarFiltreOgrenciSiparisler(String kategori, String urun, String firma,
			String siparis_baslangic, String siparis_bitis, int ogrenciId) {

		//try {
			Session session = entityManager.unwrap(Session.class);
			//SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			String sql;
			if (siparis_baslangic != "" && siparis_bitis != "") {
				//Date siparis_baslangicc = formatter.parse(siparis_baslangic.toString());
				//Date siparis_bitiss = formatter.parse(siparis_bitis.toString());
				//String s_baslangic=formatter.format(siparis_baslangicc);
				//String s_bitis=formatter.format(siparis_bitiss);
				sql = "FROM Baskilar WHERE ogrenci=" + ogrenciId
						+ " AND siparis_durumu!=8  AND urun_fiyat.urun.kategori.kategori_adi LIKE '%" + kategori
						+ "%' AND urun_fiyat.urun.urun_adi LIKE '%" + urun + "%' AND firma.firma_adi LIKE '%" + firma
						+ "%'  AND kayit_tarihi BETWEEN '" + siparis_baslangic + "' AND '" + siparis_bitis + " 23:59:59.999999'";
			} else {
				sql = "FROM Baskilar WHERE ogrenci=" + ogrenciId
						+ " AND siparis_durumu!=8 AND urun_fiyat.urun.kategori.kategori_adi LIKE '%" + kategori
						+ "%' AND urun_fiyat.urun.urun_adi LIKE '%" + urun + "%' AND firma.firma_adi LIKE '%" + firma
						+ "%' ";
			}

			List<Baskilar> baskilar = session.createQuery(sql, Baskilar.class).getResultList();
			return baskilar;
		//} catch (ParseException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			//return null;
		//}
	}

	@Override
	public List<Baskilar> getBakilarFiltreGrafiker(String kategori, String urun, String firma, String siparis_baslangic,
			String siparis_bitis, int grafikerId) {
		//try {
			Session session = entityManager.unwrap(Session.class);
			//SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			String sql;
			if (siparis_baslangic != "" && siparis_bitis != "") {
				//Date siparis_baslangicc = formatter.parse(siparis_baslangic.toString());
				//Date siparis_bitiss = formatter.parse(siparis_bitis.toString());
				sql = "FROM Baskilar WHERE grafiker=" + grafikerId
						+ " AND urun_fiyat.urun.kategori.kategori_adi LIKE '%" + kategori
						+ "%' AND urun_fiyat.urun.urun_adi LIKE '%" + urun + "%' AND firma.firma_adi LIKE '%" + firma
						+ "%' AND kayit_tarihi BETWEEN '" + siparis_baslangic + "' AND '" + siparis_bitis + " 23:59:59.999999'";
			} else {
				sql = "FROM Baskilar WHERE grafiker=" + grafikerId
						+ " AND urun_fiyat.urun.kategori.kategori_adi LIKE '%" + kategori
						+ "%' AND urun_fiyat.urun.urun_adi LIKE '%" + urun + "%' AND firma.firma_adi LIKE '%" + firma
						+ "%' ";
			}

			List<Baskilar> baskilar = session.createQuery(sql, Baskilar.class).getResultList();
			return baskilar;
		//} catch (ParseException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			//return null;
		//}
	}

	@Override
	public List<Baskilar> getBakilarFiltreTedarikci(String kategori, String urun, String firma,
			String siparis_baslangic, String siparis_bitis, int tedarikciId) {
		//try {
			Session session = entityManager.unwrap(Session.class);
			//SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			String sql;
			if (siparis_baslangic != "" && siparis_bitis != "") {
				//Date siparis_baslangicc = formatter.parse(siparis_baslangic.toString());
				//Date siparis_bitiss = formatter.parse(siparis_bitis.toString());
				sql = "FROM Baskilar WHERE urun_fiyat.urun.basim_yeri=" + tedarikciId
						+ "AND (siparis_durumu=4 OR siparis_durumu=6 OR siparis_durumu=7 OR siparis_durumu=8) AND urun_fiyat.urun.kategori.kategori_adi LIKE '%" + kategori
						+ "%' AND urun_fiyat.urun.urun_adi LIKE '%" + urun + "%' AND firma.firma_adi LIKE '%" + firma
						+ "%' AND kayit_tarihi BETWEEN '" + siparis_baslangic + "' AND '" + siparis_bitis + " 23:59:59.999999'";
			} else {
				sql = "FROM Baskilar WHERE urun_fiyat.urun.basim_yeri=" + tedarikciId
						+ " AND (siparis_durumu=4 OR siparis_durumu=6 OR siparis_durumu=7 OR siparis_durumu=8) AND urun_fiyat.urun.kategori.kategori_adi LIKE '%" + kategori
						+ "%' AND urun_fiyat.urun.urun_adi LIKE '%" + urun + "%' AND firma.firma_adi LIKE '%" + firma
						+ "%' ";
			}

			List<Baskilar> baskilar = session.createQuery(sql, Baskilar.class).getResultList();
			return baskilar;
		//} catch (ParseException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			//return null;
		//}
	}

	@Override
	@Transactional
	public void setOnay(int id) {
		Session session = entityManager.unwrap(Session.class);
		Query query = session.createQuery("UPDATE Baskilar SET siparis_durumu=3 WHERE id=" + id);
		query.executeUpdate();
		
	}

	@Override
	@Transactional
	public void revize(int id, String revizeNotu) {
		Session session = entityManager.unwrap(Session.class);
		Query query = session.createQuery("UPDATE Baskilar b SET b.siparis_durumu=5, b.revize_notu='"+revizeNotu+"'  WHERE b.id=" + id);
		query.executeUpdate();
		
	}

	@Override
	@Transactional
	public void ogrenciOnay(int id) {
		Session session = entityManager.unwrap(Session.class);
		Query query = session.createQuery("UPDATE Baskilar b SET b.siparis_durumu=4 WHERE b.id=" + id);
		query.executeUpdate();
		
	}

	@Override
	@Transactional
	public List<Baskilar> tedarikciList(int id) {
		Session session = entityManager.unwrap(Session.class);
		List<Baskilar> baskilar = session
				.createQuery("FROM Baskilar WHERE (siparis_durumu=4 OR siparis_durumu=6) AND urun_fiyat.urun.basim_yeri=" + id, Baskilar.class)
				.getResultList();
		return baskilar;
		
	}

	@Override
	@Transactional
	public void updateDurum(int siparis_durumu, int kargo_firmasi_id, String kargo_takip_no, int id) {
		Session session = entityManager.unwrap(Session.class);
		String sql="UPDATE Baskilar b SET b.siparis_durumu="+siparis_durumu;
		if(kargo_firmasi_id>0) {
			sql+=", b.kargo_firmasi="+kargo_firmasi_id;
		}
		
		sql+=", b.siparis_takip_no='"+kargo_takip_no+"'  WHERE b.id="+id;
		Query query = session.createQuery(sql);
		query.executeUpdate();
		
	}

	@Override
	public List<Baskilar> getOgrenciBaski(int id) {
		Session session = entityManager.unwrap(Session.class);
		List<Baskilar> baskilar = session
				.createQuery("FROM Baskilar WHERE sepette_mi=false and ogrenci.id=" + id, Baskilar.class)
				.getResultList();
		return baskilar;
	}

	@Override
	public List<Baskilar> getGrafikerBaski(int id) {
		Session session = entityManager.unwrap(Session.class);
		List<Baskilar> baskilar = session
				.createQuery("FROM Baskilar WHERE sepette_mi=false and grafiker.id=" + id, Baskilar.class)
				.getResultList();
		return baskilar;
	}

	@Override
	public List<Baskilar> getTedarikciBaski(int id) {
		Session session = entityManager.unwrap(Session.class);
		List<Baskilar> baskilar = session
				.createQuery("FROM Baskilar WHERE sepette_mi=false and urun_fiyat.urun.basim_yeri=" + id, Baskilar.class)
				.getResultList();
		return baskilar;
	}
	
	@Override
	@Transactional
	public void setTeslim(int id) {
		Session session = entityManager.unwrap(Session.class);
		Query query = session
				.createQuery("UPDATE Baskilar SET siparis_durumu=8 WHERE id=" +id);
		query.executeUpdate();
	}

}
