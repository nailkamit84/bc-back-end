package com.asmajans.springBootRest.DataAccess;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.asmajans.springBootRest.Entities.Okul;

@Repository
public class HibernateOkulDal implements IOkulDal {

	private EntityManager entityManager;
	
	@Autowired
	public HibernateOkulDal(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	@Override
	@Transactional
	public List<Okul> getAll() {
		Session session = entityManager.unwrap(Session.class);
		List<Okul> okul = session.createQuery("FROM Okul", Okul.class).getResultList();
		return okul;
	}

	@Override
	public Okul add(Okul okul) {
		Session session = entityManager.unwrap(Session.class);
		session.saveOrUpdate(okul);
		return okul;
	}

	@Override
	public Okul update(Okul okul) {
		Session session = entityManager.unwrap(Session.class);
		session.saveOrUpdate(okul);
		return okul;
	}

	@Override
	public Okul delete(Okul okul) {
		Session session = entityManager.unwrap(Session.class);
		Okul okulToDelete = session.get(Okul.class, okul.getId());
		session.delete(okulToDelete);
		return okul;
	}

	@Override
	public Okul getById(int id) {
		Session session = entityManager.unwrap(Session.class);
		Okul okul= session.get(Okul.class, id);
		return okul;
	}

}
