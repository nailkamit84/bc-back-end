package com.asmajans.springBootRest.DataAccess;

import org.springframework.data.jpa.repository.JpaRepository;

import com.asmajans.springBootRest.Entities.Urunler;

public interface UrunlerRepository extends JpaRepository<Urunler, Integer> {

}
