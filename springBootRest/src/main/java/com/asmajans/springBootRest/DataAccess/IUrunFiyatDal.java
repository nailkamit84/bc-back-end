package com.asmajans.springBootRest.DataAccess;

import java.util.List;

import com.asmajans.springBootRest.Entities.UrunFiyat;

public interface IUrunFiyatDal {
	List<UrunFiyat> getAll();
	void add(UrunFiyat urunFiyat);
	void update(UrunFiyat urunFiyat);
	void delete(UrunFiyat urunFiyat);
	UrunFiyat getById(int id);
	List<UrunFiyat> getbyUrun(int id);
	UrunFiyat getbyAltOZellik(int id,String AltOzellik);

}
