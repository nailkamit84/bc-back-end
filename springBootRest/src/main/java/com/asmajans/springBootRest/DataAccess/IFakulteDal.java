package com.asmajans.springBootRest.DataAccess;


import java.util.List;

import com.asmajans.springBootRest.Entities.Fakulte;

public interface IFakulteDal {

	List<Fakulte> getById(int id);

}
