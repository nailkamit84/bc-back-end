package com.asmajans.springBootRest.DataAccess;

import java.util.List;

import com.asmajans.springBootRest.Entities.Kullanicilar;


public interface IKullanicilarDal {
	
	List<Object[]> getOgrenciler();
	List<Object[]> getTedarikciler();
	List<Object[]> getGrafikerler();
	Kullanicilar getById(int id);

}
