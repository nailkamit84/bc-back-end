package com.asmajans.springBootRest.DataAccess;

import java.util.List;

import com.asmajans.springBootRest.Entities.Kampanyalar;

public interface IKampanyalarDal {
	List<Kampanyalar> getAll();
	Kampanyalar add(Kampanyalar kampanya);

}
