package com.asmajans.springBootRest.DataAccess;

import java.util.List;

import com.asmajans.springBootRest.Entities.Ozellikler;;

public interface IOzelliklerDal {
	List<Ozellikler> getAll();
	Ozellikler add(Ozellikler kategori);
	void update(Ozellikler kategori);
	void delete(Ozellikler kategori);
	List<Ozellikler> getById(int id);
	Ozellikler infoId(int id);
}
