package com.asmajans.springBootRest.Business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.asmajans.springBootRest.DataAccess.IUrunFiyatDal;
import com.asmajans.springBootRest.Entities.UrunFiyat;

@Service
public class UrunFiyatManager implements IUrunFiyatService {
	
	private IUrunFiyatDal urunFiyatDal;
	
	@Autowired
	public UrunFiyatManager(IUrunFiyatDal urunFiyatDal) {
		this.urunFiyatDal = urunFiyatDal;
		
	}

	@Override
	@Transactional
	public List<UrunFiyat> getAll() {
		return this.urunFiyatDal.getAll();
	}

	@Override
	public void add(UrunFiyat urunFiyat) {
		this.urunFiyatDal.add(urunFiyat);
		
	}

	@Override
	public void update(UrunFiyat urunFiyat) {
		this.urunFiyatDal.update(urunFiyat);
		
	}

	@Override
	public void delete(UrunFiyat urunFiyat) {
		this.urunFiyatDal.delete(urunFiyat);
		
	}

	@Override
	public UrunFiyat getById(int id) {
		return this.urunFiyatDal.getById(id);
	}

	@Override
	public List<UrunFiyat> getbyUrun(int id) {
		return this.urunFiyatDal.getbyUrun(id);
	}

	@Override
	public UrunFiyat getbyAltOZellik(int id, String AltOzellik) {
		return this.urunFiyatDal.getbyAltOZellik(id, AltOzellik);
	}

}
