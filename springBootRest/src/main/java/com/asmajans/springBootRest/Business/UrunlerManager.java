package com.asmajans.springBootRest.Business;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.asmajans.springBootRest.DataAccess.IKategoriDal;
import com.asmajans.springBootRest.DataAccess.IUrunlerDal;
import com.asmajans.springBootRest.Entities.Urunler;

@Service
public class UrunlerManager implements IUrunlerService {

	private IUrunlerDal urunDal;
	private IKategoriDal kategoriDal; 
	
	@Autowired
	public UrunlerManager(IUrunlerDal urunDal, IKategoriDal kategoriDal) {
		this.urunDal = urunDal;
		this.kategoriDal=kategoriDal;
	}
	
	@Override
	@Transactional
	public List<Urunler> getAll() {
		return this.urunDal.getAll();
	}

	@Override
	@Transactional
	public Urunler add(Urunler urun) {
		System.out.println(urun.getKategori().getId());
		Date date=new Date();
		urun.setKayit_tarihi(date);
		urun.setKategori(kategoriDal.getById(urun.getKategori().getId()));
		System.out.println(urun.getKategori().getKategori_adi());
		return this.urunDal.add(urun);
		
	}

	@Override
	@Transactional
	public void update(Urunler urun) {
		this.urunDal.update(urun);
		
	}

	@Override
	@Transactional
	public void delete(Urunler urun) {
		this.urunDal.delete(urun);
		
	}

	@Override
	public Urunler getById(int id) {
		return this.urunDal.getById(id);
	}

	@Override
	public List<Urunler> getYetki(int id) {
		return this.urunDal.getYetki(id);
	}

	@Override
	public List<Urunler> yetkiBos() {
		return this.urunDal.yetkiBos();
	}

	@Override
	public void setYetkiDuzenle(String urunler, int tedarikciId) {
		this.urunDal.setYetkiDuzenle(urunler, tedarikciId);
		
	}

	@Override
	public List<Urunler> getbyKategori(int id) {
		return this.urunDal.getbyKategori(id);
	}

}
