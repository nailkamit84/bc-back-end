package com.asmajans.springBootRest.Business;

import java.util.List;

import com.asmajans.springBootRest.Entities.KargoFirmalari;

public interface IKargoFirmalariService {
	
	List<KargoFirmalari> getAll();

}
