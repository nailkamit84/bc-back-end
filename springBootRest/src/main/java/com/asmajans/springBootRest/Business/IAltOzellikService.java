package com.asmajans.springBootRest.Business;

import java.util.List;

import com.asmajans.springBootRest.Entities.AltOzellik;


public interface IAltOzellikService {
	List<AltOzellik> getAll();
	void add(AltOzellik altOzellik);
	void update(AltOzellik altOzellik);
	void delete(AltOzellik altOzellik);
	List<AltOzellik> getById(int id);
	List<AltOzellik> getByUrun(int urunId);
	AltOzellik infoId(int id);

}
