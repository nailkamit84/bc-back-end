package com.asmajans.springBootRest.Business;

import java.util.List;

import com.asmajans.springBootRest.Entities.Urunler;

public interface IUrunlerService {
	List<Urunler> getAll();
	Urunler add(Urunler urun);
	void update(Urunler urun);
	void delete(Urunler urun);
	Urunler getById(int id);
	List<Urunler> getYetki(int id);
	List<Urunler> yetkiBos();
	void setYetkiDuzenle(String urunler, int tedarikciId);
	List<Urunler> getbyKategori(int id);

}
