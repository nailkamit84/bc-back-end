package com.asmajans.springBootRest.Business;

import java.util.List;

import com.asmajans.springBootRest.Entities.Kullanicilar;



public interface IKullanicilarService {
	
	List<Object[]> getOgrenciler();
	List<Object[]> getTedarikciler();
	List<Object[]> getGrafikerler();
	Kullanicilar getById(int id);

}
