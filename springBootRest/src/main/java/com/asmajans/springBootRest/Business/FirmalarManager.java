package com.asmajans.springBootRest.Business;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.asmajans.springBootRest.DataAccess.IFirmalarDal;
import com.asmajans.springBootRest.DataAccess.IKullanicilarDal;
import com.asmajans.springBootRest.Entities.Firmalar;

@Service
public class FirmalarManager implements IFirmalarService {
	
	private IFirmalarDal firmalarDal;
	private IKullanicilarDal kullanicilarDal;
	
	@Autowired
	public FirmalarManager(IFirmalarDal firmalarDal, IKullanicilarDal kullanicilarDal) {
		this.firmalarDal = firmalarDal;
		this.kullanicilarDal = kullanicilarDal;
	}

	@Override
	@Transactional
	public List<Firmalar> getAll() {
		return this.firmalarDal.getAll();
	}

	@Override
	public Firmalar add(Firmalar firmalar) {
		Date date=new Date();
		firmalar.setKayit_tarihi(date);
		firmalar.setOgrenci(kullanicilarDal.getById(firmalar.getOgrenci().getId()));
		return this.firmalarDal.add(firmalar);
		
	}

	@Override
	public void update(Firmalar firmalar) {
		this.firmalarDal.delete(firmalar);
		
	}

	@Override
	public void delete(Firmalar firmalar) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Firmalar> getById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Firmalar infoId(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Firmalar> getbyOgrenci(int id) {
		return this.firmalarDal.getbyOgrenci(id);
	}

}
