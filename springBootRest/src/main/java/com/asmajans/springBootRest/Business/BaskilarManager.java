package com.asmajans.springBootRest.Business;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.asmajans.springBootRest.DataAccess.IBaskilarDal;
import com.asmajans.springBootRest.Entities.Baskilar;

@Service
public class BaskilarManager implements IBaskilarService {

	private IBaskilarDal baskiDal;

	@Autowired
	public BaskilarManager(IBaskilarDal baskiDal) {
		this.baskiDal = baskiDal;
	}

	@Override
	@Transactional
	public List<Baskilar> getAll() {
		return this.baskiDal.getAll();
	}

	@Override
	public Baskilar add(Baskilar baski) {
		return this.baskiDal.add(baski);
	}

	@Override
	public void update(String url, int id) {
		this.baskiDal.update(url, id);
				
	}

	@Override
	public Baskilar delete(Baskilar baski) {
		return this.baskiDal.delete(baski);
	}

	@Override
	public Baskilar getById(int id) {
		return this.baskiDal.getById(id);
	}

	@Override
	public List<Baskilar> getSepet(int id) {
		return this.baskiDal.getSepet(id);
	}

	@Override
	public void listedenCikar(int id) {
		this.baskiDal.listedenCikar(id);
		
	}

	@Override
	public void sepetOnay(Baskilar baski) {
		Date date=new Date();
		baski.setKayit_tarihi(date);
		this.baskiDal.sepetOnay(baski);
	}

	@Override
	public List<Baskilar> tasarimAl(int id) {
		
		return this.baskiDal.tasarimAl(id);
	}

	@Override
	public List<Baskilar> getBaskilarFiltre(String kategoriId, String urunId, String firmaId, String ogrenciId, String grafikerId,
			String tedarikciId, String siparis_baslangic, String siparis_bitis) {
		return this.baskiDal.getBaskilarFiltre(kategoriId, urunId, firmaId, ogrenciId, grafikerId, tedarikciId, siparis_baslangic, siparis_bitis);
	}

	@Override
	public List<Baskilar> getBakilarFiltreOgrenci(String kategori, String urun, String firma, String siparis_baslangic,
			String siparis_bitis, int ogrenciId) {
		return this.baskiDal.getBakilarFiltreOgrenci(kategori, urun, firma, siparis_baslangic, siparis_bitis, ogrenciId);
	}

	@Override
	public List<Baskilar> getBakilarFiltreOgrenciSiparisler(String kategori, String urun, String firma,
			String siparis_baslangic, String siparis_bitis, int ogrenciId) {
		
		return this.baskiDal.getBakilarFiltreOgrenciSiparisler(kategori, urun, firma, siparis_baslangic, siparis_bitis, ogrenciId);
	}

	@Override
	public List<Baskilar> getBakilarFiltreGrafiker(String kategori, String urun, String firma, String siparis_baslangic,
			String siparis_bitis, int grafikerId) {
		
		return this.baskiDal.getBakilarFiltreGrafiker(kategori, urun, firma, siparis_baslangic, siparis_bitis, grafikerId);
	}

	@Override
	public List<Baskilar> getBakilarFiltreTedarikci(String kategori, String urun, String firma,
			String siparis_baslangic, String siparis_bitis, int tedarikciId) {

return this.baskiDal.getBakilarFiltreTedarikci(kategori, urun, firma, siparis_baslangic, siparis_bitis, tedarikciId);
	}

	@Override
	public void setOnay(int id) {
		this.baskiDal.setOnay(id);
		
	}

	@Override
	public void revize(int id, String revizeNotu) {
		this.baskiDal.revize(id, revizeNotu);
		
	}

	@Override
	public void ogrenciOnay(int id) {
		this.baskiDal.ogrenciOnay(id);
		
	}

	@Override
	public List<Baskilar> tedarikciList(int id) {
		return this.baskiDal.tedarikciList(id);
	}

	@Override
	public void updateDurum(int siparis_durumu, int kargo_firmasi_id, String kargo_takip_no, int id) {
		this.baskiDal.updateDurum(siparis_durumu, kargo_firmasi_id, kargo_takip_no, id);
		
	}

	@Override
	public List<Baskilar> getOgrenciBaski(int id) {
		return this.baskiDal.getOgrenciBaski(id);
	}

	@Override
	public List<Baskilar> getGrafikerBaski(int id) {
		return this.baskiDal.getGrafikerBaski(id);
	}

	@Override
	public List<Baskilar> getTedarikciBaski(int id) {
		return this.baskiDal.getTedarikciBaski(id);
	}

	@Override
	public void setTeslim(int id) {
		this.baskiDal.setTeslim(id);
		
	}

}
