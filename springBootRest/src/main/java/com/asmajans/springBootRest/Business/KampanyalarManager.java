package com.asmajans.springBootRest.Business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.asmajans.springBootRest.DataAccess.IKampanyalarDal;

import com.asmajans.springBootRest.Entities.Kampanyalar;

@Service
public class KampanyalarManager implements IKampanyalarService {

	private IKampanyalarDal kampanyalarDal;
	
	@Autowired
	public KampanyalarManager(IKampanyalarDal kampanyalarDal) {
		this.kampanyalarDal = kampanyalarDal;
	}
	
	@Override
	public List<Kampanyalar> getAll() {
		return this.kampanyalarDal.getAll();
	}

	@Override
	public Kampanyalar add(Kampanyalar kampanya) {
		return this.kampanyalarDal.add(kampanya);
	}

}
