package com.asmajans.springBootRest.Business;

import java.util.List;

import com.asmajans.springBootRest.Entities.Fakulte;

public interface IFakulteService {
	List<Fakulte> getById(int id);

}
