package com.asmajans.springBootRest.Business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.asmajans.springBootRest.DataAccess.IKargoFirmalariDal;
import com.asmajans.springBootRest.Entities.KargoFirmalari;

@Service
public class KargoFirmalariManager implements IKargoFirmalariService {

	private IKargoFirmalariDal kargoFirmalariDal;
	
	@Autowired
	public KargoFirmalariManager(IKargoFirmalariDal kargoFirmalariDal) {
		this.kargoFirmalariDal = kargoFirmalariDal;
	}
	
	@Override
	public List<KargoFirmalari> getAll() {
		return kargoFirmalariDal.getAll();
	}

}
