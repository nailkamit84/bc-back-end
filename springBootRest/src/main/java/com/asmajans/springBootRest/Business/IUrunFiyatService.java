package com.asmajans.springBootRest.Business;

import java.util.List;


import com.asmajans.springBootRest.Entities.UrunFiyat;

public interface IUrunFiyatService {
	List<UrunFiyat> getAll();
	void add(UrunFiyat urunFiyat);
	void update(UrunFiyat urunFiyat);
	void delete(UrunFiyat urunFiyat);
	UrunFiyat getById(int id);
	List<UrunFiyat> getbyUrun(int id);
	UrunFiyat getbyAltOZellik(int id,String AltOzellik);

}
