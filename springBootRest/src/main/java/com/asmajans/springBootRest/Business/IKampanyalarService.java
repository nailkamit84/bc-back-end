package com.asmajans.springBootRest.Business;

import java.util.List;

import com.asmajans.springBootRest.Entities.Kampanyalar;

public interface IKampanyalarService {
	List<Kampanyalar> getAll();
	Kampanyalar add(Kampanyalar kampanya);

}
