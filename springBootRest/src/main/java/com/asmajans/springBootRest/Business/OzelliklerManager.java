package com.asmajans.springBootRest.Business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.asmajans.springBootRest.DataAccess.IOzelliklerDal;
import com.asmajans.springBootRest.Entities.Ozellikler;

@Service
public class OzelliklerManager implements IOzelliklerService {

	private IOzelliklerDal ozellikDal;
	
	@Autowired
	public OzelliklerManager(IOzelliklerDal ozellikDal) {
		this.ozellikDal = ozellikDal;
	}
	
	@Override
	@Transactional
	public List<Ozellikler> getAll() {
		return this.ozellikDal.getAll();
	}

	@Override
	public Ozellikler add(Ozellikler ozellik) {
	
		return this.ozellikDal.add(ozellik);
		
	}

	@Override
	@Transactional
	public void update(Ozellikler ozellik) {
		// TODO Auto-generated method stub
		
	}

	@Override
	@Transactional
	public void delete(Ozellikler ozellik) {
		// TODO Auto-generated method stub
		
	}

	@Override
	@Transactional
	public List<Ozellikler> getById(int id) {
		return this.ozellikDal.getById(id);
	}

	@Override
	@Transactional
	public Ozellikler infoId(int id) {
		return this.ozellikDal.infoId(id);
	}

}
