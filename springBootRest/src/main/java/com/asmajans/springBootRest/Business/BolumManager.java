package com.asmajans.springBootRest.Business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.asmajans.springBootRest.DataAccess.IBolumDal;
import com.asmajans.springBootRest.DataAccess.IFakulteDal;
import com.asmajans.springBootRest.Entities.Bolum;

@Service
public class BolumManager implements IBolumService {

	private IBolumDal bolumDal;
	
	@Autowired
	public BolumManager(IBolumDal bolumDal, IFakulteDal fakulteDal) {
		this.bolumDal = bolumDal;
	}
	
	

	@Override
	@Transactional
	public List<Bolum> getById(int id) {
		return this.bolumDal.getById(id);
	}



	@Override
	public Bolum infoGetId(int id) {
		return this.bolumDal.infoGetId(id);
	}

}
