package com.asmajans.springBootRest.Business;

import java.util.List;

import com.asmajans.springBootRest.Entities.Bolum;

public interface IBolumService {

	List<Bolum> getById(int id);
	Bolum infoGetId(int id);

}
