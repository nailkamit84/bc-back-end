package com.asmajans.springBootRest.Business;

import java.util.List;

import com.asmajans.springBootRest.Entities.Firmalar;


public interface IFirmalarService {

	List<Firmalar> getAll();
	Firmalar add(Firmalar firmalar);
	void update(Firmalar firmalar);
	void delete(Firmalar firmalar);
	List<Firmalar> getById(int id);
	Firmalar infoId(int id);
	List<Firmalar> getbyOgrenci(int id);
}
