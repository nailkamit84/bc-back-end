package com.asmajans.springBootRest.Business;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.asmajans.springBootRest.DataAccess.IAltOzellikDal;
import com.asmajans.springBootRest.DataAccess.IOzelliklerDal;
import com.asmajans.springBootRest.Entities.AltOzellik;

@Service
public class AltOzelliklerManager implements IAltOzellikService {

	private IAltOzellikDal altOzellikDal;
	private IOzelliklerDal ozelliklerDal;
	
	@Autowired
	public AltOzelliklerManager(IAltOzellikDal altOzellikDal, IOzelliklerDal ozelliklerDal) {
		this.altOzellikDal = altOzellikDal;
		this.ozelliklerDal = ozelliklerDal;
	}
	
	@Override
	@Transactional
	public List<AltOzellik> getAll() {
		return this.altOzellikDal.getAll();
	}

	@Override
	@Transactional
	public void add(AltOzellik altOzellik) {
		Date date=new Date();
		altOzellik.setKayit_tarihi(date);
		altOzellik.setOzellik(ozelliklerDal.infoId(altOzellik.getOzellik().getId()));
		
		this.altOzellikDal.add(altOzellik);
		
	}

	@Override
	@Transactional
	public void update(AltOzellik altOzellik) {
		Date date=new Date();
		altOzellik.setKayit_tarihi(date);
		altOzellik.setOzellik(ozelliklerDal.infoId(altOzellik.getOzellik().getId()));
		
		this.altOzellikDal.add(altOzellik);
		
	}

	@Override
	@Transactional
	public void delete(AltOzellik altOzellik) {
		// TODO Auto-generated method stub
		
	}

	@Override
	@Transactional
	public List<AltOzellik> getById(int id) {
		return this.altOzellikDal.getById(id);
	}

	@Override
	public AltOzellik infoId(int id) {
		return this.altOzellikDal.infoId(id);
	}

	@Override
	public List<AltOzellik> getByUrun(int id) {
		return this.altOzellikDal.getByUrun(id);
	}

}
