package com.asmajans.springBootRest.Business;

import java.util.List;

import com.asmajans.springBootRest.Entities.Baskilar;

public interface IBaskilarService {
	List<Baskilar> getAll();

	Baskilar add(Baskilar baski);

	void update(String url, int id);

	Baskilar delete(Baskilar baski);

	Baskilar getById(int id);

	List<Baskilar> getSepet(int id);

	void listedenCikar(int id);

	void sepetOnay(Baskilar baski);

	List<Baskilar> tasarimAl(int id);

	List<Baskilar> getBaskilarFiltre(String kategoriId, String urunId, String firmaId, String ogrenciId,
			String grafikerId, String tedarikciId, String siparis_baslangic, String siparis_bitis);

	List<Baskilar> getBakilarFiltreOgrenci(String kategori, String urun, String firma, String siparis_baslangic,
			String siparis_bitis, int ogrenciId);

	List<Baskilar> getBakilarFiltreOgrenciSiparisler(String kategori, String urun, String firma,
			String siparis_baslangic, String siparis_bitis, int ogrenciId);

	List<Baskilar> getBakilarFiltreGrafiker(String kategori, String urun, String firma, String siparis_baslangic,
			String siparis_bitis, int grafikerId);

	List<Baskilar> getBakilarFiltreTedarikci(String kategori, String urun, String firma, String siparis_baslangic,
			String siparis_bitis, int tedarikciId);

	void setOnay(int id);

	void revize(int id, String revizeNotu);

	void ogrenciOnay(int id);
	
	List<Baskilar> tedarikciList(int id);
	void updateDurum(int siparis_durumu, int kargo_firmasi_id, String kargo_takip_no, int id);
	
	List<Baskilar> getOgrenciBaski(int id);
	
	List<Baskilar> getGrafikerBaski(int id);
	
	List<Baskilar> getTedarikciBaski(int id);
	
	void setTeslim(int id);

}
