package com.asmajans.springBootRest.Business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.asmajans.springBootRest.DataAccess.IKategoriDal;
import com.asmajans.springBootRest.Entities.Kategoriler;

@Service
public class KategoriManager implements IKategorilerService {

	private IKategoriDal kategoriDal;
	
	@Autowired
	public KategoriManager(IKategoriDal kategoriDal) {
		this.kategoriDal = kategoriDal;
		
	}
	
	@Override
	@Transactional
	public List<Kategoriler> getAll() {
		return this.kategoriDal.getAll();
	}

	@Override
	public void add(Kategoriler kategori) {
		this.kategoriDal.add(kategori);
		
	}

	@Override
	public void update(Kategoriler kategori) {
		this.kategoriDal.update(kategori);
		
	}

	@Override
	public void delete(Kategoriler kategori) {
		this.kategoriDal.delete(kategori);
		
	}

	@Override
	public Kategoriler getById(int id) {
		return this.kategoriDal.getById(id);
	}

}
