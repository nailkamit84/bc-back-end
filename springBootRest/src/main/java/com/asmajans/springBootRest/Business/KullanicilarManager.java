package com.asmajans.springBootRest.Business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.asmajans.springBootRest.DataAccess.IKullanicilarDal;
import com.asmajans.springBootRest.Entities.Kullanicilar;


@Service
public class KullanicilarManager implements IKullanicilarService {

	private IKullanicilarDal kullanicilarDal;
	
	@Autowired
	public KullanicilarManager(IKullanicilarDal kullanicilarDal) {
		this.kullanicilarDal = kullanicilarDal;
	}
	
	@Override
	@Transactional
	public List<Object[]> getOgrenciler() {
		return kullanicilarDal.getOgrenciler();
	}

	@Override
	@Transactional
	public List<Object[]> getTedarikciler() {
		return kullanicilarDal.getTedarikciler();
	}

	@Override
	@Transactional
	public List<Object[]> getGrafikerler() {
		return kullanicilarDal.getGrafikerler();
	}

	@Override
	public Kullanicilar getById(int id) {
		return kullanicilarDal.getById(id);
	}

}
