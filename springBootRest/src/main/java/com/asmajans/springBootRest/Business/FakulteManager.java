package com.asmajans.springBootRest.Business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.asmajans.springBootRest.DataAccess.IFakulteDal;
import com.asmajans.springBootRest.Entities.Fakulte;

@Service
public class FakulteManager implements IFakulteService {

	private IFakulteDal fakulteDal;
	
	@Autowired
	public FakulteManager(IFakulteDal fakulteDal) {
		this.fakulteDal = fakulteDal;
	}
	
	@Override
	@Transactional
	public List<Fakulte> getById(int id) {
		return fakulteDal.getById(id);
	}
	

}
