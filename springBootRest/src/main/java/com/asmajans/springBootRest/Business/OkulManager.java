package com.asmajans.springBootRest.Business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.asmajans.springBootRest.DataAccess.IOkulDal;
import com.asmajans.springBootRest.Entities.Okul;

@Service
public class OkulManager implements IOkulService {

	private IOkulDal okulDal;
	
	@Autowired
	public OkulManager(IOkulDal okulDal) {
		this.okulDal = okulDal;
	}
	
	@Override
	@Transactional
	public List<Okul> getAll() {
		return okulDal.getAll();
	}

	@Override
	public Okul add(Okul okul) {
		return okulDal.add(okul);
	}

	@Override
	public Okul update(Okul okul) {
		return okulDal.update(okul);
	}

	@Override
	public Okul delete(Okul okul) {
		return okulDal.delete(okul);
	}

	@Override
	public Okul getById(int id) {
		return okulDal.getById(id);
	}

}
