package com.asmajans.springBootRest.Entities;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "urunler")
public class Urunler {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "urun_adi")
	private String urun_adi;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "kategori", referencedColumnName = "id")
	private Kategoriler kategori;

	@Column(name = "Resim_url")
	private String resim_url;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "urun")
	private List<Ozellikler> ozellik;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "urun")
	private List<UrunFiyat> urunFiyat;
	
	@Column(name = "sirasi")
	private int sirasi;

	public Kullanicilar getBasim_yeri() {
		return basim_yeri;
	}

	public void setBasim_yeri(Kullanicilar basim_yeri) {
		this.basim_yeri = basim_yeri;
	}

	@Column(name = "kayit_tarihi")
	private Date kayit_tarihi;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "basim_yeri", referencedColumnName = "id")
	private Kullanicilar basim_yeri;

	public String getUrun_adi() {
		return urun_adi;
	}

	public void setUrun_adi(String urun_adi) {
		this.urun_adi = urun_adi;
	}

	public int getSirasi() {
		return sirasi;
	}

	public void setSirasi(int sirasi) {
		this.sirasi = sirasi;
	}

	public Urunler() {

	}

	public Urunler(int id, String urun_adi, Kategoriler kategori, String resim_url, int sirasi, Date kayit_tarihi) {
		super();
		this.id = id;
		this.urun_adi = urun_adi;
		this.kategori = kategori;
		this.resim_url = resim_url;
		this.sirasi = sirasi;
		this.kayit_tarihi = kayit_tarihi;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Kategoriler getKategori() {
		return kategori;
	}

	public void setKategori(Kategoriler kategori) {
		this.kategori = kategori;
	}

	public String getResim_url() {
		return resim_url;
	}

	public void setResim_url(String resim_url) {
		this.resim_url = resim_url;
	}

	public Date getKayit_tarihi() {
		return kayit_tarihi;
	}

	public void setKayit_tarihi(Date date) {

		this.kayit_tarihi = date;
	}

}
