package com.asmajans.springBootRest.Entities;

public enum ERole {
	ROLE_ADMIN,
	ROLE_OGRENCI,
	ROLE_TEDARIKCI,
	ROLE_GRAFIKER

}
