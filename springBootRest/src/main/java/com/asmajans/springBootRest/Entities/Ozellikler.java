package com.asmajans.springBootRest.Entities;


import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "ozellikler")
public class Ozellikler {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "ozellik_adi", nullable = false)
	private String ozellik_adi;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="urun_id", referencedColumnName = "id", nullable = false)
	private Urunler urun;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "ozellik")
	private List<AltOzellik> altOzellik;

	public Urunler getUrun() {
		return urun;
	}

	public void setUrun(Urunler urun) {
		this.urun = urun;
	}

	public Ozellikler(int id, String ozellik_adi, Urunler urun) {
		super();
		this.id = id;
		this.ozellik_adi = ozellik_adi;
		this.urun = urun;
		
	}
	public Ozellikler() {
	
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getOzellik_adi() {
		return ozellik_adi;
	}

	public void setOzellik_adi(String ozellik_adi) {
		this.ozellik_adi = ozellik_adi;
	}

	
}

	