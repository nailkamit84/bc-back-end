package com.asmajans.springBootRest.Entities;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="firmalar")
public class Firmalar {
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name="firma_adi")
	private String firma_adi;
	
	@Column(name="firma_adres")
	private String firma_adres;
	
	@Column(name="firma_telefon")
	private String firma_telefon;
	
	@Column(name="tc_kimlik_no")
	private String tc_kimlik_no;
	
	@Column(name="vergi_dairesi")
	private String vergi_dairesi;
	
	@Column(name="vergi_numarasi")
	private String vergi_numarasi;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="ogrenci_id", referencedColumnName = "id")
	private Kullanicilar ogrenci;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "firma")
	List<Baskilar> baskilar;
	
	@Column(name="kayit_tarihi")
	private Date kayit_tarihi;
	public Firmalar(int id, String firma_adi, String firma_adres, String firma_telefon, String tc_kimlik_no,
			String vergi_dairesi, String vergi_numarasi, Kullanicilar ogrenci, Date kayit_tarihi) {
		super();
		this.id = id;
		this.firma_adi = firma_adi;
		this.firma_adres = firma_adres;
		this.firma_telefon = firma_telefon;
		this.tc_kimlik_no = tc_kimlik_no;
		this.vergi_dairesi = vergi_dairesi;
		this.vergi_numarasi = vergi_numarasi;
		this.ogrenci = ogrenci;
		this.kayit_tarihi = kayit_tarihi;
	}

	public Firmalar() {
		
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirma_adi() {
		return firma_adi;
	}

	public void setFirma_adi(String firma_adi) {
		this.firma_adi = firma_adi;
	}

	public String getFirma_adres() {
		return firma_adres;
	}

	public void setFirma_adres(String firma_adres) {
		this.firma_adres = firma_adres;
	}

	public String getFirma_telefon() {
		return firma_telefon;
	}

	public void setFirma_telefon(String firma_telefon) {
		this.firma_telefon = firma_telefon;
	}

	public String getTc_kimlik_no() {
		return tc_kimlik_no;
	}

	public void setTc_kimlik_no(String tc_kimlik_no) {
		this.tc_kimlik_no = tc_kimlik_no;
	}

	public String getVergi_dairesi() {
		return vergi_dairesi;
	}

	public void setVergi_dairesi(String vergi_dairesi) {
		this.vergi_dairesi = vergi_dairesi;
	}

	public String getVergi_numarasi() {
		return vergi_numarasi;
	}

	public void setVergi_numarasi(String vergi_numarasi) {
		this.vergi_numarasi = vergi_numarasi;
	}

	public Kullanicilar getOgrenci() {
		return ogrenci;
	}

	public void setOgrenci(Kullanicilar ogrenci) {
		this.ogrenci = ogrenci;
	}

	public Date getKayit_tarihi() {
		return kayit_tarihi;
	}

	public void setKayit_tarihi(Date kayit_tarihi) {
		this.kayit_tarihi = kayit_tarihi;
	}
	

}
