package com.asmajans.springBootRest.Entities;

import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="kategoriler")
public class Kategoriler {
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="kategori_adi")
	private String kategori_adi;
	
	@Column(name="sirasi")
	private int sirasi;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "kategori")
	private List<Urunler> urun;

	public Kategoriler(int id, String kategori_adi, int sirasi) {
		super();
		this.id = id;
		this.kategori_adi = kategori_adi;
		this.sirasi = sirasi;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getKategori_adi() {
		return kategori_adi;
	}

	public void setKategori_adi(String kategori_adi) {
		this.kategori_adi = kategori_adi;
	}
	public Kategoriler() {
		
	}

	public int getSirasi() {
		return sirasi;
	}

	public void setSirasi(int sirasi) {
		this.sirasi = sirasi;
	}
	
	

}
