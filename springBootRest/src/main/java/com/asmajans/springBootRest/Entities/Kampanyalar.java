package com.asmajans.springBootRest.Entities;

import javax.persistence.*;

@Entity
@Table(name="kampanyalar")
public class Kampanyalar {
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name="slide_url")
	private String slide_url;
	
	@Column(name="sirasi")
	private int sirasi;
	
	public Kampanyalar() {
		
	}

	public Kampanyalar(int id, String slide_url, int sirasi) {
		super();
		this.id = id;
		this.slide_url = slide_url;
		this.sirasi = sirasi;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSlide_url() {
		return slide_url;
	}

	public void setSlide_url(String slide_url) {
		this.slide_url = slide_url;
	}

	public int getSirasi() {
		return sirasi;
	}

	public void setSirasi(int sirasi) {
		this.sirasi = sirasi;
	}
}

	