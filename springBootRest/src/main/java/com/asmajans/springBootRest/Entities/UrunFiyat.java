package com.asmajans.springBootRest.Entities;


import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="urun_fiyat")
public class UrunFiyat {
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="urun_id", referencedColumnName = "id")
	private Urunler urun;

	@Column(name="alis_fiyati")
	private double alisFiyati;
	
	@Column(name="satis_fiyati")
	private double satis_fiyati;
	
	@Column(name="ogrenci_kar_payi")
	private double ogrenci_kar_payi;
	
	@Column(name="indirim")
	private double indirim;
	
	@Column(name="alt_ozellikler")
	private String alt_ozellikler;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "urun_fiyat")
	List<Baskilar> baskilar;

	public String getAlt_ozellikler() {
		return alt_ozellikler;
	}

	public void setAlt_ozellikler(String alt_ozellikler) {
		this.alt_ozellikler = alt_ozellikler;
	}
	public UrunFiyat() {
		
	}

	public UrunFiyat(int id, Urunler urun,  double alisFiyati, double satis_fiyati,
			double ogrenci_kar_payi, double indirim, String alt_ozellikler) {
		super();
		this.id = id;
		this.urun = urun;
		this.alisFiyati = alisFiyati;
		this.satis_fiyati = satis_fiyati;
		this.ogrenci_kar_payi = ogrenci_kar_payi;
		this.indirim = indirim;
		this.alt_ozellikler= alt_ozellikler;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Urunler getUrun() {
		return urun;
	}

	public void setUrun(Urunler urun) {
		this.urun = urun;
	}

	public double getAlisFiyati() {
		return alisFiyati;
	}

	public void setAlisFiyati(double alisFiyati) {
		this.alisFiyati = alisFiyati;
	}

	public double getSatis_fiyati() {
		return satis_fiyati;
	}

	public void setSatis_fiyati(double satis_fiyati) {
		this.satis_fiyati = satis_fiyati;
	}

	public double getOgrenci_kar_payi() {
		return ogrenci_kar_payi;
	}

	public void setOgrenci_kar_payi(double ogrenci_kar_payi) {
		this.ogrenci_kar_payi = ogrenci_kar_payi;
	}

	public double getIndirim() {
		return indirim;
	}

	public void setIndirim(double indirim) {
		this.indirim = indirim;
	}
	
	

}
