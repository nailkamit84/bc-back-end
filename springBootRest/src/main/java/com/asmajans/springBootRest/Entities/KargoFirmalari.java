package com.asmajans.springBootRest.Entities;

import javax.persistence.*;

@Entity
@Table(name="kargo_firmalari")
public class KargoFirmalari {
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name="kargo_firma_adi")
	private String kargo_firma_adi;
	
	public KargoFirmalari() {
		
	}

	public KargoFirmalari(int id, String kargo_firma_adi) {
		super();
		this.id = id;
		this.kargo_firma_adi = kargo_firma_adi;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getKargo_firma_adi() {
		return kargo_firma_adi;
	}

	public void setKargo_firma_adi(String kargo_firma_adi) {
		this.kargo_firma_adi = kargo_firma_adi;
	}

	
	
}
