package com.asmajans.springBootRest.Entities;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name="kullanicilar", uniqueConstraints = { 
		@UniqueConstraint(columnNames = "username"),
		@UniqueConstraint(columnNames = "email") 
	})
public class Kullanicilar {
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name="ad_soyad")
	private String ad_soyad;
	
	@Column(name="email")
	private String email;
	
	@Column(name="telefon")
	private String telefon;
	
	public Date getDogum_tarihi() {
		return dogum_tarihi;
	}

	public void setDogum_tarihi(Date dogum_tarihi) {
		this.dogum_tarihi = dogum_tarihi;
	}
	@Column(name="dogum_tarihi")
	private Date dogum_tarihi;
	
	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name="bolum_id", referencedColumnName = "id")
	private Bolum bolum;
	
	@Column(name="username")
	private String username;
	
	@Column(name="password")
	private String password;
	
	@Column(name="profil_resmi")
	private String profil_resmi;
	
	@Column(name="durum")
	private Boolean durum;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "ogrenci")
	List<Baskilar> ogrenciBaski;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "grafiker")
	List<Baskilar> grafikerBaski;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "tedarikci")
	List<Baskilar> tedarikciBaski;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "basim_yeri")
	private List<Urunler> basim;
	
	public String getAdres() {
		return adres;
	}

	public void setAdres(String adres) {
		this.adres = adres;
	}
	@Column(name="adres")
	private String adres;

	@ManyToMany(cascade = CascadeType.PERSIST)
	@JoinTable(name="kullanici_unvan", joinColumns = @JoinColumn(name="kullanici_id"), inverseJoinColumns = @JoinColumn(name = "unvan_id"))
	private Set<Unvanlar> unvan;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "ogrenci")
	private List<Firmalar> firma;
	
	@Column(name="kayit_tarihi")
	private Date kayit_tarihi;
	
	public Set<Unvanlar> getUnvan() {
		return unvan;
	}

	public void setUnvan(Set<Unvanlar> unvan) {
		this.unvan = unvan;
	}

	public Kullanicilar(String username, String email, String password) {

		this.email = email;
		
		this.username = username;
		this.password = password;

	}
	
	public Kullanicilar() {
		// TODO Auto-generated constructor stub
	}
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAd_soyad() {
		return ad_soyad;
	}
	public void setAd_soyad(String ad_soyad) {
		this.ad_soyad = ad_soyad;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelefon() {
		return telefon;
	}
	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}
	public Bolum getBolum() {
		return bolum;
	}
	public void setBolum(Bolum bolum) {
		this.bolum = bolum;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public Date getKayit_tarihi() {
		return kayit_tarihi;
	}
	public void setKayit_tarihi(Date kayit_tarihi) {
		this.kayit_tarihi = kayit_tarihi;
	}


}
