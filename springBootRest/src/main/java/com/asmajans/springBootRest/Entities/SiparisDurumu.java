package com.asmajans.springBootRest.Entities;


import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="siparis_durumu")
public class SiparisDurumu {
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name="durum")
	private String durum;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "siparis_durumu")
	List<Baskilar> siparisDurumu;

	public SiparisDurumu(int id, String durum) {
		super();
		this.id = id;
		this.durum = durum;
	}
	public SiparisDurumu() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDurum() {
		return durum;
	}

	public void setDurum(String durum) {
		this.durum = durum;
	}

	
	
	

}
