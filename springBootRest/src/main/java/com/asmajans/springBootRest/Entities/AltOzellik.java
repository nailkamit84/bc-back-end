package com.asmajans.springBootRest.Entities;

import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name="alt_ozellik")
public class AltOzellik {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="alt_ozel_adi")
	private String alt_ozel_adi;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="ozellik_id", referencedColumnName = "id")
	private Ozellikler ozellik;
	
	@Column(name="kayit_tarihi")
	private Date kayit_tarihi;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAlt_ozel_adi() {
		return alt_ozel_adi;
	}
	public void setAlt_ozel_adi(String alt_ozel_adi) {
		this.alt_ozel_adi = alt_ozel_adi;
	}
	public Ozellikler getOzellik() {
		return ozellik;
	}
	public void setOzellik(Ozellikler ozellik) {
		this.ozellik = ozellik;
	}
	public Date getKayit_tarihi() {
		return kayit_tarihi;
	}
	public void setKayit_tarihi(Date kayit_tarihi) {
		this.kayit_tarihi = kayit_tarihi;
	}
	public AltOzellik(int id, String alt_ozel_adi, Ozellikler ozellik, Date kayit_tarihi) {
		super();
		this.id = id;
		this.alt_ozel_adi = alt_ozel_adi;
		this.ozellik = ozellik;
		this.kayit_tarihi = kayit_tarihi;
	}
	public AltOzellik() {
		
	}

}
