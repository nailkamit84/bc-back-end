package com.asmajans.springBootRest.Entities;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="unvanlar")
public class Unvanlar {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Enumerated(EnumType.STRING)
	@Column(name="unvan_adi")
	private ERole name;
	
	@Column(name="kayit_tarihi")
	private Date kayit_tarihi;
	
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST, mappedBy = "unvan")
	private List<Kullanicilar> kullanici;
	
	public Unvanlar() {
		
	}
	
	public Unvanlar(int id, ERole unvan_adi, Date kayit_tarihi) {
		this.id = id;
		this.name = unvan_adi;
		this.kayit_tarihi = kayit_tarihi;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public ERole getUnvan_adi() {
		return name;
	}
	public void setUnvan_adi(ERole unvan_adi) {
		this.name = unvan_adi;
	}
	public Date getKayit_tarihi() {
		return kayit_tarihi;
	}
	public void setKayit_tarihi(Date kayit_tarihi) {
		this.kayit_tarihi = kayit_tarihi;
	}
	
}
