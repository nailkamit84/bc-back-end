package com.asmajans.springBootRest.Entities;

import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="fakulte")
public class Fakulte {
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name="fakulte_adi")
	private String fakulte_adi;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="okul_id", referencedColumnName = "id")
	private Okul okul;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "fakulte")
	private List<Bolum> bolum;
	
	public Fakulte() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFakulte_adi() {
		return fakulte_adi;
	}

	public void setFakulte_adi(String fakulte_adi) {
		this.fakulte_adi = fakulte_adi;
	}

	public Okul getOkul() {
		return okul;
	}

	public void setOkul(Okul okul) {
		this.okul = okul;
	}

	public Fakulte(int id, String fakulte_adi, Okul okul) {
		super();
		this.id = id;
		this.fakulte_adi = fakulte_adi;
		this.okul = okul;
	}
	
	

}
