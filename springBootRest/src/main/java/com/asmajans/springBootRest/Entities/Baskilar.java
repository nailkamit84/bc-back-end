package com.asmajans.springBootRest.Entities;

import java.util.Date;


import javax.persistence.*;

@Entity
@Table(name = "baskilar")
public class Baskilar {
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "siparis_kodu")
	private String siparisKodu;

	@ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ogrenci_id", referencedColumnName = "id")
	private Kullanicilar ogrenci;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "grafiker_id", referencedColumnName = "id")
	private Kullanicilar grafiker;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "tedarikci_id", referencedColumnName = "id")
	private Kullanicilar tedarikci;

	@Column(name = "grafikere_gidis_tarihi")
	private Date grafikere_gidis_tarihi;

	@Column(name = "tedarikciye_gidis_tarihi")
	private Date tedarikciye_gidis_tarihi;

	@Column(name = "sepette_mi")
	private Boolean sepette_mi;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "siparis_durumu_id", referencedColumnName = "id")
	private SiparisDurumu siparis_durumu;
	
	@Column(name="ogrenci_onay")
	private Boolean ogrenci_onay=false;
	
	@Column(name="ogrenci_onay_tarihi")
	private Date ogrenci_onay_tarihi;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "urun_fiyat_id", referencedColumnName = "id")
	private UrunFiyat urun_fiyat;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "firma_id", referencedColumnName = "id")
	private Firmalar firma;

	@Column(name = "tasarim_url")
	private String tasarim_url;

	@Column(name = "aciklama")
	private String aciklama;

	@Column(name = "gorsel_url")
	private String gorseller_url;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "kargo_firmasi_id", referencedColumnName = "id")
	private KargoFirmalari kargo_firmasi;

	@Column(name = "siparis_takip_no")
	private String siparis_takip_no;

	@Column(name = "revize_sayisi")
	private int revize_sayisi=0;
	
	@Column(name="revize_notu")
	private String revize_notu;
	
	

	@Column(name="kayit_tarihi")
	private Date kayit_tarihi;

	public String getRevize_notu() {
		return revize_notu;
	}

	public void setRevize_notu(String revize_notu) {
		this.revize_notu = revize_notu;
	}
	
	public Boolean getOgrenci_onay() {
		return ogrenci_onay;
	}

	public void setOgrenci_onay(Boolean ogrenci_onay) {
		this.ogrenci_onay = ogrenci_onay;
	}

	public Date getOgrenci_onay_tarihi() {
		return ogrenci_onay_tarihi;
	}

	public void setOgrenci_onay_tarihi(Date ogrenci_onay_tarihi) {
		this.ogrenci_onay_tarihi = ogrenci_onay_tarihi;
	}

	
	public Date getKayit_tarihi() {
		return kayit_tarihi;
	}

	public void setKayit_tarihi(Date kayit_tarihi) {
		this.kayit_tarihi = kayit_tarihi;
	}

	
	
	public Baskilar() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


	public String getSiparisKodu() {
		return siparisKodu;
	}

	public void setSiparisKodu(String siparisKodu) {
		this.siparisKodu = siparisKodu;
	}


	public Kullanicilar getOgrenci() {
		return ogrenci;
	}

	public void setOgrenci(Kullanicilar ogrenci) {
		this.ogrenci = ogrenci;
	}

	public Kullanicilar getGrafiker() {
		return grafiker;
	}

	public void setGrafiker(Kullanicilar grafiker) {
		this.grafiker = grafiker;
	}

	public Kullanicilar getTedarikci() {
		return tedarikci;
	}

	public void setTedarikci(Kullanicilar tedarikci) {
		this.tedarikci = tedarikci;
	}

	public Date getGrafikere_gidis_tarihi() {
		return grafikere_gidis_tarihi;
	}

	public void setGrafikere_gidis_tarihi(Date grafikere_gidis_tarihi) {
		this.grafikere_gidis_tarihi = grafikere_gidis_tarihi;
	}


	public Date getTedarikciye_gidis_tarihi() {
		return tedarikciye_gidis_tarihi;
	}

	public void setTedarikciye_gidis_tarihi(Date tedarikciye_gidis_tarihi) {
		this.tedarikciye_gidis_tarihi = tedarikciye_gidis_tarihi;
	}


	public Boolean getSepette_mi() {
		return sepette_mi;
	}

	public void setSepette_mi(Boolean sepette_mi) {
		this.sepette_mi = sepette_mi;
	}

	public SiparisDurumu getSiparis_durumu() {
		return siparis_durumu;
	}

	public void setSiparis_durumu(SiparisDurumu siparis_durumu) {
		this.siparis_durumu = siparis_durumu;
	}

	public UrunFiyat getUrun_fiyat() {
		return urun_fiyat;
	}

	public void setUrun_fiyat(UrunFiyat urun_fiyat) {
		this.urun_fiyat = urun_fiyat;
	}

	public Firmalar getFirma() {
		return firma;
	}

	public void setFirma(Firmalar firma) {
		this.firma = firma;
	}

	public String getTasarim_url() {
		return tasarim_url;
	}

	public void setTasarim_url(String tasarim_url) {
		this.tasarim_url = tasarim_url;
	}

	public String getAciklama() {
		return aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public String getGorseller_url() {
		return gorseller_url;
	}

	public void setGorseller_url(String gorseller_url) {
		this.gorseller_url = gorseller_url;
	}

	public KargoFirmalari getKargo_firmasi() {
		return kargo_firmasi;
	}

	public void setKargo_firmasi(KargoFirmalari kargo_firmasi) {
		this.kargo_firmasi = kargo_firmasi;
	}

	public String getSiparis_takip_no() {
		return siparis_takip_no;
	}

	public void setSiparis_takip_no(String siparis_takip_no) {
		this.siparis_takip_no = siparis_takip_no;
	}

	public int getRevize_sayisi() {
		return revize_sayisi;
	}

	public void setRevize_sayisi(int revize_sayisi) {
		this.revize_sayisi = revize_sayisi;
	}

	public Baskilar(int id, String siparisKodu, Kullanicilar ogrenci, Kullanicilar grafiker, Kullanicilar tedarikci,
			Date grafikere_gidis_tarihi, Date tedarikciye_gidis_tarihi, Boolean sepette_mi,
			SiparisDurumu siparis_durumu, Boolean ogrenci_onay, Date ogrenci_onay_tarihi, UrunFiyat urun_fiyat,
			Firmalar firma, String tasarim_url, String aciklama, String gorseller_url, KargoFirmalari kargo_firmasi,
			String siparis_takip_no, int revize_sayisi, String revize_notu, Date kayit_tarihi) {
		super();
		this.id = id;
		this.siparisKodu = siparisKodu;
		this.ogrenci = ogrenci;
		this.grafiker = grafiker;
		this.tedarikci = tedarikci;
		this.grafikere_gidis_tarihi = grafikere_gidis_tarihi;
		this.tedarikciye_gidis_tarihi = tedarikciye_gidis_tarihi;
		this.sepette_mi = sepette_mi;
		this.siparis_durumu = siparis_durumu;
		this.ogrenci_onay = ogrenci_onay;
		this.ogrenci_onay_tarihi = ogrenci_onay_tarihi;
		this.urun_fiyat = urun_fiyat;
		this.firma = firma;
		this.tasarim_url = tasarim_url;
		this.aciklama = aciklama;
		this.gorseller_url = gorseller_url;
		this.kargo_firmasi = kargo_firmasi;
		this.siparis_takip_no = siparis_takip_no;
		this.revize_sayisi = revize_sayisi;
		this.revize_notu = revize_notu;
		this.kayit_tarihi = kayit_tarihi;
	}

	
}
