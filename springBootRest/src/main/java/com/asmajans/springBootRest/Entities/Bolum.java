package com.asmajans.springBootRest.Entities;

import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="bolum")
public class Bolum {
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name="bolum_adi")
	private String bolum_adi;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="fakulte_id", referencedColumnName = "id")
	private Fakulte fakulte;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "bolum")
	private List<Kullanicilar> kullanicilar;
	
	public Bolum() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBolum_adi() {
		return bolum_adi;
	}

	public void setBolum_adi(String bolum_adi) {
		this.bolum_adi = bolum_adi;
	}

	public Fakulte getFakulte() {
		return fakulte;
	}

	public void setFakulte(Fakulte fakulte) {
		this.fakulte = fakulte;
	}

	public Bolum(int id, String bolum_adi, Fakulte fakulte) {
		super();
		this.id = id;
		this.bolum_adi = bolum_adi;
		this.fakulte = fakulte;
	}
	
	

}
