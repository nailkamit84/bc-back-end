package com.asmajans.springBootRest.Entities;


import javax.persistence.*;

@Entity
@Table(name="okul")
public class Okul {
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name="okul_adi")
	private String okul_adi;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getOkul_adi() {
		return okul_adi;
	}

	public void setOkul_adi(String okul_adi) {
		this.okul_adi = okul_adi;
	}
	public Okul() {
		
	}

	public Okul(int id, String okul_adi) {
		super();
		this.id = id;
		this.okul_adi = okul_adi;
	}

}
	
	